package io.xmu.dataanalysis.model;

/**
 * @author lepdou 2017-04-16
 */
public class ShopMonitorModel {

    private int mainShopId;
    private String monitorShopName;

    public int getMainShopId() {
        return mainShopId;
    }

    public void setMainShopId(int mainShopId) {
        this.mainShopId = mainShopId;
    }

    public String getMonitorShopName() {
        return monitorShopName;
    }

    public void setMonitorShopName(String monitorShopName) {
        this.monitorShopName = monitorShopName;
    }
}
