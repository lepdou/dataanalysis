package io.xmu.dataanalysis.model;

/**
 * @author lepdou 2017-03-20
 */
public class MockModel {

  private String partition;
  private int pvSize;
  private int orderSize;
  private int favoriteSize;
  private int shoppingCartSize;
  private int searchSize;

  public String getPartition() {
    return partition;
  }

  public void setPartition(String partition) {
    this.partition = partition;
  }

  public int getPvSize() {
    return pvSize;
  }

  public void setPvSize(int pvSize) {
    this.pvSize = pvSize;
  }

  public int getOrderSize() {
    return orderSize;
  }

  public void setOrderSize(int orderSize) {
    this.orderSize = orderSize;
  }

  public int getFavoriteSize() {
    return favoriteSize;
  }

  public void setFavoriteSize(int favoriteSize) {
    this.favoriteSize = favoriteSize;
  }

  public int getShoppingCartSize() {
    return shoppingCartSize;
  }

  public void setShoppingCartSize(int shoppingCartSize) {
    this.shoppingCartSize = shoppingCartSize;
  }

  public int getSearchSize() {
    return searchSize;
  }

  public void setSearchSize(int searchSize) {
    this.searchSize = searchSize;
  }
}
