package io.xmu.dataanalysis.model;

import java.util.List;

/**
 * @author lepdou 2017-04-16
 */
public class ShopMonitorVO {

    private Integer shopId;
    private String shopName;

    private Counter ySales;
    private Counter saleNum;
    private Counter saleMoney;
    private Counter words;
    private Counter tofu;

    private List<String> top3Goods;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Counter getySales() {
        return ySales;
    }

    public void setySales(Counter ySales) {
        this.ySales = ySales;
    }

    public Counter getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Counter saleNum) {
        this.saleNum = saleNum;
    }

    public Counter getSaleMoney() {
        return saleMoney;
    }

    public void setSaleMoney(Counter saleMoney) {
        this.saleMoney = saleMoney;
    }

    public Counter getWords() {
        return words;
    }

    public void setWords(Counter words) {
        this.words = words;
    }

    public Counter getTofu() {
        return tofu;
    }

    public void setTofu(Counter tofu) {
        this.tofu = tofu;
    }

    public List<String> getTop3Goods() {
        return top3Goods;
    }

    public void setTop3Goods(List<String> top3Goods) {
        this.top3Goods = top3Goods;
    }

    public static class Counter {

        private double count;
        private boolean isUp;
        private double scale;

        public double getCount() {
            return count;
        }

        public void setCount(double count) {
            this.count = count;
        }

        public boolean isUp() {
            return isUp;
        }

        public void setUp(boolean up) {
            isUp = up;
        }

        public double getScale() {
            return scale;
        }

        public void setScale(double scale) {
            this.scale = scale;
        }
    }

}
