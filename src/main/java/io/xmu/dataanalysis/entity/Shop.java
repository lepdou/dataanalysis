package io.xmu.dataanalysis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-02-25
 */
@Entity
@Table(name = "Shop")
public class Shop extends BaseEntity {

  @Column(name = "Name")
  private String name;
  @Column(name = "Pic")
  private String pic;
  @Column(name = "CateId")
  private int cateId;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic;
  }

  public int getCateId() {
    return cateId;
  }

  public void setCateId(int cateId) {
    this.cateId = cateId;
  }
}
