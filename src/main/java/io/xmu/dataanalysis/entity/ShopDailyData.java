package io.xmu.dataanalysis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ShopDailyData")
public class ShopDailyData extends BaseEntity {

  @Column(name = "ShopId")
  private int shopId;
  @Column(name = "CateId")
  private int cateId;
  @Column(name = "Sales")
  private long sales;
  @Column(name = "CreateOrderCount")
  private int createOrderCount;
  @Column(name = "payedGoodsCount")
  private int payedGoodsCount;
  @Column(name = "ViewGoodsCount")
  private int viewGoodsCount;
  @Column(name = "payedUserCount")
  private int payedUserCount;
  @Column(name = "pv")
  private int pv;
  @Column(name = "VisitOnlyOnePageUsers")
  private int visitOnlyOnePageUsers;
  @Column(name = "PVByApp")
  private int pvByApp;
  @Column(name = "uv")
  private int uv;
  @Column(name = "UVByApp")
  private int uvByApp;
  @Column(name = "AddShoppingCartCount")
  private int addShoppingCartCount;
  @Column(name = "AvgStayTime")
  private int avgStayTime;
  @Column(name = "partition")
  private String partition;

  public int getShopId() {
    return shopId;
  }

  public void setShopId(int shopId) {
    this.shopId = shopId;
  }

  public int getCateId() {
    return cateId;
  }

  public void setCateId(int cateId) {
    this.cateId = cateId;
  }

  public long getSales() {
    return sales;
  }

  public void setSales(long sales) {
    this.sales = sales;
  }

  public int getPayedGoodsCount() {
    return payedGoodsCount;
  }

  public void setPayedGoodsCount(int payedGoodsCount) {
    this.payedGoodsCount = payedGoodsCount;
  }

  public int getPayedUserCount() {
    return payedUserCount;
  }

  public void setPayedUserCount(int payedUserCount) {
    this.payedUserCount = payedUserCount;
  }

  public int getPv() {
    return pv;
  }

  public void setPv(int pv) {
    this.pv = pv;
  }

  public int getUv() {
    return uv;
  }

  public void setUv(int uv) {
    this.uv = uv;
  }

  public String getPartition() {
    return partition;
  }

  public void setPartition(String partition) {
    this.partition = partition;
  }

  public int getPvByApp() {
    return pvByApp;
  }

  public void setPvByApp(int pvByApp) {
    this.pvByApp = pvByApp;
  }

  public int getUvByApp() {
    return uvByApp;
  }

  public void setUvByApp(int uvByApp) {
    this.uvByApp = uvByApp;
  }

  public int getAvgStayTime() {
    return avgStayTime;
  }

  public void setAvgStayTime(int avgStayTime) {
    this.avgStayTime = avgStayTime;
  }

  public int getVisitOnlyOnePageUsers() {
    return visitOnlyOnePageUsers;
  }

  public void setVisitOnlyOnePageUsers(int visitOnlyOnePageUsers) {
    this.visitOnlyOnePageUsers = visitOnlyOnePageUsers;
  }

  public int getAddShoppingCartCount() {
    return addShoppingCartCount;
  }

  public void setAddShoppingCartCount(int addShoppingCartCount) {
    this.addShoppingCartCount = addShoppingCartCount;
  }

  public int getViewGoodsCount() {
    return viewGoodsCount;
  }

  public void setViewGoodsCount(int viewGoodsCount) {
    this.viewGoodsCount = viewGoodsCount;
  }

  public int getCreateOrderCount() {
    return createOrderCount;
  }

  public void setCreateOrderCount(int createOrderCount) {
    this.createOrderCount = createOrderCount;
  }
}
