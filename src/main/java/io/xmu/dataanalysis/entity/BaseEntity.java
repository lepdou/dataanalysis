package io.xmu.dataanalysis.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

/**
 * @author lepdou 2017-02-25
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class BaseEntity {

  @Id
  @GeneratedValue
  @Column(name = "Id")
  private int id;

  @Column(name = "IsDeleted", columnDefinition = "Bit default '0'")
  protected boolean isDeleted = false;

  @Column(name = "DataChange_LastModifiedBy")
  private String modifiedBy;

  @Column(name = "DataChange_LastTime")
  private Date modifiedTime;


  public String getModifiedBy() {
    return modifiedBy;
  }

  public Date getModifiedTime() {
    return modifiedTime;
  }

  public int getId() {
    return id;
  }

  public boolean isDeleted() {
    return isDeleted;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public void setModifiedTime(Date modifiedTime) {
    this.modifiedTime = modifiedTime;
  }

  public void setDeleted(boolean deleted) {
    isDeleted = deleted;
  }

  public void setId(int id) {
    this.id = id;
  }

  @PrePersist
  protected void prePersist() {
    if (this.modifiedTime == null) {
      modifiedTime = new Date();
    }
    if (this.modifiedBy == null) {
      this.modifiedBy = "admin";
    }
  }

  @PreUpdate
  protected void preUpdate() {
    this.modifiedTime = new Date();
  }

  @PreRemove
  protected void preRemove() {
    this.modifiedTime = new Date();
  }

}
