package io.xmu.dataanalysis.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-02-25
 */
@Entity
@Table(name = "Order")
public class Order extends BaseEntity {

  @Column(name = "GoodsId")
  private int goodsId;
  @Column(name = "UserId")
  private int userId;
  @Column(name = "Status")
  private int status;
  @Column(name = "Client")
  private int client;
  @Column(name = "CreateTime")
  private Date createTime;
  @Column(name = "PayTime")
  private Date payTime;

  public int getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(int goodsId) {
    this.goodsId = goodsId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public int getClient() {
    return client;
  }

  public void setClient(int client) {
    this.client = client;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Date getPayTime() {
    return payTime;
  }

  public void setPayTime(Date payTime) {
    this.payTime = payTime;
  }

}
