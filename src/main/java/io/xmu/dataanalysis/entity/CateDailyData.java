package io.xmu.dataanalysis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-03-13
 */
@Entity
@Table(name = "CateDailyData")
public class CateDailyData extends BaseEntity {

  @Column(name = "cateId")
  private int cateId;
  @Column(name = "sales")
  private long sales;
  @Column(name = "payedGoodsCount")
  private int payedGoodsCount;
  @Column(name = "payedUserCount")
  private int payedUserCount;
  @Column(name = "pv")
  private int pv;
  @Column(name = "UV")
  private int uv;
  @Column(name = "partition")
  private String partition;

  public int getCateId() {
    return cateId;
  }

  public void setCateId(int cateId) {
    this.cateId = cateId;
  }

  public long getSales() {
    return sales;
  }

  public void setSales(long sales) {
    this.sales = sales;
  }

  public int getPayedGoodsCount() {
    return payedGoodsCount;
  }

  public void setPayedGoodsCount(int payedGoodsCount) {
    this.payedGoodsCount = payedGoodsCount;
  }

  public int getPayedUserCount() {
    return payedUserCount;
  }

  public void setPayedUserCount(int payedUserCount) {
    this.payedUserCount = payedUserCount;
  }

  public int getPv() {
    return pv;
  }

  public void setPv(int pv) {
    this.pv = pv;
  }

  public int getUv() {
    return uv;
  }

  public void setUv(int uv) {
    this.uv = uv;
  }

  public String getPartition() {
    return partition;
  }

  public void setPartition(String partition) {
    this.partition = partition;
  }
}
