package io.xmu.dataanalysis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-03-12
 */
@Entity
@Table(name = "GoodsDailyData")
public class GoodsDailyData extends BaseEntity {

  @Column(name = "goodsId")
  private int goodsId;
  @Column(name = "ShopId")
  private int shopId;
  @Column(name = "CateId")
  private int cateId;
  @Column(name = "favoriteCount")
  private int favoriteCount;
  @Column(name = "addToCartCount")
  private int addToCartCount;
  @Column(name = "payCount")
  private int payCount;
  @Column(name = "payMoneyCount")
  private long payMoneyCount;
  @Column(name = "pv")
  private int pv;
  @Column(name = "partition")
  private String partition;

  public int getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(int goodsId) {
    this.goodsId = goodsId;
  }

  public int getShopId() {
    return shopId;
  }

  public void setShopId(int shopId) {
    this.shopId = shopId;
  }

  public int getCateId() {
    return cateId;
  }

  public void setCateId(int cateId) {
    this.cateId = cateId;
  }

  public int getFavoriteCount() {
    return favoriteCount;
  }

  public void setFavoriteCount(int favoriteCount) {
    this.favoriteCount = favoriteCount;
  }

  public int getAddToCartCount() {
    return addToCartCount;
  }

  public void setAddToCartCount(int addToCartCount) {
    this.addToCartCount = addToCartCount;
  }

  public int getPayCount() {
    return payCount;
  }

  public void setPayCount(int payCount) {
    this.payCount = payCount;
  }

  public long getPayMoneyCount() {
    return payMoneyCount;
  }

  public void setPayMoneyCount(long payMoneyCount) {
    this.payMoneyCount = payMoneyCount;
  }

  public int getPv() {
    return pv;
  }

  public void setPv(int pv) {
    this.pv = pv;
  }

  public String getPartition() {
    return partition;
  }

  public void setPartition(String partition) {
    this.partition = partition;
  }
}
