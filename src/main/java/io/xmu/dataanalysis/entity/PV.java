package io.xmu.dataanalysis.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-02-25
 */
@Entity
@Table(name = "PV")
public class PV extends BaseEntity {

  @Column(name = "UserId")
  private int userId;
  @Column(name = "ShopId")
  private int shopId;
  @Column(name = "Type")
  private int type;
  @Column(name = "Client")
  private int client;
  @Column(name = "GoodsId")
  private int goodsId;
  @Column(name = "ViewTime")
  private int ViewTime;
  @Column(name = "CreateTime")
  private Date createTime;

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getShopId() {
    return shopId;
  }

  public void setShopId(int shopId) {
    this.shopId = shopId;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public int getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(int goodsId) {
    this.goodsId = goodsId;
  }

  public int getViewTime() {
    return ViewTime;
  }

  public void setViewTime(int viewTime) {
    ViewTime = viewTime;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public int getClient() {
    return client;
  }

  public void setClient(int client) {
    this.client = client;
  }
}
