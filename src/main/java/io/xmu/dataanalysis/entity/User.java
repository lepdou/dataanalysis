package io.xmu.dataanalysis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-02-25
 */
@Entity
@Table(name = "User")
public class User extends BaseEntity {

  @Column(name = "Name")
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
