package io.xmu.dataanalysis.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-02-25
 */
@Entity
@Table(name = "Search")
public class Search extends BaseEntity {

  @Column(name = "CateId")
  private int cateId;
  @Column(name = "Key")
  private String key;
  @Column(name = "SearchTime")
  private Date searchTime;

  public int getCateId() {
    return cateId;
  }

  public void setCateId(int cateId) {
    this.cateId = cateId;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public Date getSearchTime() {
    return searchTime;
  }

  public void setSearchTime(Date searchTime) {
    this.searchTime = searchTime;
  }
}
