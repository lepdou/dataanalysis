package io.xmu.dataanalysis.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-02-25
 */
@Entity
@Table(name = "Favorite")
public class Favorite extends BaseEntity {

  @Column(name = "GoodsId")
  private int goodsId;
  @Column(name = "UserId")
  private int userId;
  @Column(name = "CreateTime")
  private Date createTime;

  public int getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(int goodsId) {
    this.goodsId = goodsId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }
}
