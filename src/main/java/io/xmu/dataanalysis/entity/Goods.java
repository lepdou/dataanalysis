package io.xmu.dataanalysis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-02-25
 */
@Entity
@Table(name = "Goods")
public class Goods extends BaseEntity {

  @Column(name = "Name")
  private String name;
  @Column(name = "ShopId")
  private int shopId;
  @Column(name = "Price")
  private int price;
  @Column(name = "Pic")
  private String pic;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getShopId() {
    return shopId;
  }

  public void setShopId(int shopId) {
    this.shopId = shopId;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic;
  }
}
