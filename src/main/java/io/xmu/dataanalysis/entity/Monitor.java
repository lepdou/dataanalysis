package io.xmu.dataanalysis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lepdou 2017-04-16
 */
@Entity
@Table(name = "Monitor")
public class Monitor extends BaseEntity {

    @Column(name = "MainShopId")
    private int mainShopId;
    @Column(name = "MonitorShopId")
    private int monitorShopId;

    public int getMainShopId() {
        return mainShopId;
    }

    public void setMainShopId(int mainShopId) {
        this.mainShopId = mainShopId;
    }

    public int getMonitorShopId() {
        return monitorShopId;
    }

    public void setMonitorShopId(int monitorShopId) {
        this.monitorShopId = monitorShopId;
    }


    @Override
    public String toString() {
        return "Monitor{" +
                "mainShopId=" + mainShopId +
                ", monitorShopId=" + monitorShopId +
                '}';
    }
}
