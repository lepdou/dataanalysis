package io.xmu.dataanalysis.configuration;

import io.xmu.dataanalysis.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Jim Chen at XMU on 2017/5/9.
 */
@Component
public class LoginFilter implements Filter {

    private static final String USER_ID = "USER_ID";

    private LoginService loginService;

    @Autowired
    public LoginFilter(LoginService loginService) {
        this.loginService = loginService;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String url = request.getRequestURL().toString();
        System.out.println(url);

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                String name = c.getName();
                if (name.equals(USER_ID)) {
                    if (url.endsWith("/login.html")) {
                        response.sendRedirect("/");
                        return;
                    } else if (url.endsWith("/logout")) {
                        c.setMaxAge(0);
                        response.addCookie(c);
                        response.setCharacterEncoding("UTF-8");
                        response.setContentType("application/json; charset=utf-8");
                        PrintWriter out = response.getWriter();
                        out.write("{\"message\":\"退出成功\"," +
                                "\"success\":\"true\"}");
                        out.close();
                        System.out.println("success");
                        return;
                    }
                    filterChain.doFilter(servletRequest,servletResponse);
                    return;
                }
            }
        }
        if (url.endsWith("/favicon.ico")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }


        if (url.endsWith("/loginPost")) {
            String account = request.getParameter("account");
            String password = request.getParameter("password");
            if (loginService.check(account, password)) {
                Cookie cookie = new Cookie(USER_ID, loginService.getCountId(account));
                response.addCookie(cookie);
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json; charset=utf-8");
                PrintWriter out = response.getWriter();
                out.write("{\"success\":\"true\"}");
                out.close();
                System.out.println("success");
            } else {
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json; charset=utf-8");
                PrintWriter out = response.getWriter();
                out.write("{\"message\":\"用户名不存在或者密码错误!\"," +
                        "\"success\":\"false\"}");
                out.close();
                System.out.println("fail");
            }
            return;
        }

        if (!url.endsWith("/login.html")) {
            response.sendRedirect("/login.html");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }

}
