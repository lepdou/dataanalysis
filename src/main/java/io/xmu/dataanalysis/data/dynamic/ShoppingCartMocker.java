package io.xmu.dataanalysis.data.dynamic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

import io.xmu.dataanalysis.data.BaseDataCache;
import io.xmu.dataanalysis.entity.ShoppingCart;
import io.xmu.dataanalysis.repository.ShoppingCartRepo;
import io.xmu.dataanalysis.util.RandomUtils;

/**
 * @author lepdou 2017-02-27
 */
@Component
public class ShoppingCartMocker implements Mocker {

  @Autowired
  private BaseDataCache baseDataCache;
  @Autowired
  private ShoppingCartRepo shoppingCartRepo;


  public void mock(String dateToDay, int size) {

    List<ShoppingCart> shoppingCarts = new LinkedList<>();

    for (int i = 0; i < size; i++) {
      ShoppingCart shoppingCart = new ShoppingCart();
      shoppingCart.setGoodsId(baseDataCache.randomGoods().getId());
      shoppingCart.setUserId(baseDataCache.randomUser().getId());
      shoppingCart.setCreateTime(RandomUtils.randomDate(dateToDay));
      shoppingCarts.add(shoppingCart);
    }

    shoppingCartRepo.save(shoppingCarts);
  }
}
