package io.xmu.dataanalysis.data.dynamic;


public interface Mocker {

  void mock(String dateToDay, int size);

}
