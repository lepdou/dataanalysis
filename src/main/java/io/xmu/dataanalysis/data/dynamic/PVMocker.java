package io.xmu.dataanalysis.data.dynamic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import io.xmu.dataanalysis.data.BaseDataCache;
import io.xmu.dataanalysis.data.OrderClient;
import io.xmu.dataanalysis.data.PageType;
import io.xmu.dataanalysis.entity.PV;
import io.xmu.dataanalysis.repository.PVRepo;
import io.xmu.dataanalysis.util.RandomUtils;

/**
 * @author lepdou 2017-02-27
 */
@Component
public class PVMocker implements Mocker {

  @Autowired
  private BaseDataCache baseDataCache;
  @Autowired
  private PVRepo pvRepo;


  public void mock(String dateToDay, int size) {
    if (size <= 0) {
      return;
    }

    List<PV> pvs = new ArrayList<PV>(size);

    for (int i = 0; i < size; i++) {
      PV pv = new PV();

      pv.setShopId(baseDataCache.randomShop().getId());
      pv.setType(PageType.random().value());
      pv.setClient(OrderClient.random().value());
      if (pv.getType() == PageType.GOODS.value()) {
        pv.setGoodsId(baseDataCache.randomGoods().getId());
      }
      pv.setUserId(baseDataCache.randomUser().getId());
      pv.setViewTime(RandomUtils.randomInt(100));
      pv.setCreateTime(RandomUtils.randomDate(dateToDay));

      pvs.add(pv);
    }

    pvRepo.save(pvs);

  }
}
