package io.xmu.dataanalysis.data.dynamic;

import io.xmu.dataanalysis.data.OrderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.xmu.dataanalysis.data.BaseDataCache;
import io.xmu.dataanalysis.data.OrderStatus;
import io.xmu.dataanalysis.entity.Order;
import io.xmu.dataanalysis.repository.OrderRepo;
import io.xmu.dataanalysis.util.RandomUtils;

/**
 * @author lepdou 2017-02-27
 */
@Component
public class OrderMocker implements Mocker {

  @Autowired
  private OrderRepo orderRepo;
  @Autowired
  private BaseDataCache baseDataCache;

  public void mock(String dateToDay, int size) {
    if (size <= 0) {
      return;
    }

    List<Order> orders = new ArrayList<Order>(size);

    for (int i = 0; i < size; i++) {
      Order order = new Order();
      order.setUserId(baseDataCache.randomUser().getId());
      order.setGoodsId(baseDataCache.randomGoods().getId());
      order.setCreateTime(RandomUtils.randomDate(dateToDay));
      order.setStatus(OrderStatus.random().value());
      order.setClient(OrderClient.random().value());

      if (order.getStatus() != OrderStatus.NEW.value()) {
        order.setPayTime(RandomUtils.randomDate(dateToDay));
      }

      orders.add(order);
    }

    orderRepo.save(orders);

  }
}
