package io.xmu.dataanalysis.data.dynamic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

import io.xmu.dataanalysis.data.BaseDataCache;
import io.xmu.dataanalysis.entity.Favorite;
import io.xmu.dataanalysis.repository.FavoriteRepo;
import io.xmu.dataanalysis.util.RandomUtils;

/**
 * @author lepdou 2017-02-27
 */
@Component
public class FavoriteMocker implements Mocker {

  @Autowired
  private FavoriteRepo favoriteRepo;
  @Autowired
  private BaseDataCache dataCache;


  public void mock(String dateToDay, int size) {
    if (size <= 0) {
      return;
    }

    List<Favorite> favorites = new LinkedList<>();

    for (int i = 0; i < size; i++) {
      Favorite favorite = new Favorite();

      favorite.setGoodsId(dataCache.randomGoods().getId());
      favorite.setUserId(dataCache.randomUser().getId());
      favorite.setCreateTime(RandomUtils.randomDate(dateToDay));

      favorites.add(favorite);
    }

    favoriteRepo.save(favorites);
  }
}
