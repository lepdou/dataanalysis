package io.xmu.dataanalysis.data;

import io.xmu.dataanalysis.util.RandomUtils;

/**
 * Created by Jim Chen at XMU on 2017/3/5.
 */
public enum OrderClient {

    MOBILE(100), WEB(200);

    private int value;

    OrderClient(int value) {
        this.value = value;
    }

    public int value(){
        return value;
    }

    public static OrderClient random(){
        int random = RandomUtils.randomInt(10);

        if(random>=4){//60%
            return MOBILE;
        }else{
            return WEB;//40%
        }
    }
}
