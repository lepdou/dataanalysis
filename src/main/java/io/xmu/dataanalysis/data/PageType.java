package io.xmu.dataanalysis.data;

import io.xmu.dataanalysis.util.RandomUtils;

public enum PageType {

  INDEX(1), RECOMMEND(2), GOODS(3);

  private int value;

  private PageType(int value) {
    this.value = value;
  }

  public int value() {
    return value;
  }

  public static PageType random() {
    int random = RandomUtils.randomInt(5);

    if (random >= 2) {
      return GOODS;
    }

    return PageType.values()[random];
  }
}
