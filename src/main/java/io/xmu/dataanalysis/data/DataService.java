package io.xmu.dataanalysis.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

import io.xmu.dataanalysis.entity.Favorite;
import io.xmu.dataanalysis.entity.Order;
import io.xmu.dataanalysis.entity.PV;
import io.xmu.dataanalysis.entity.ShoppingCart;
import io.xmu.dataanalysis.repository.FavoriteRepo;
import io.xmu.dataanalysis.repository.OrderRepo;
import io.xmu.dataanalysis.repository.PVRepo;
import io.xmu.dataanalysis.repository.ShoppingCartRepo;
import io.xmu.dataanalysis.util.DateUtils;

/**
 * @author lepdou 2017-03-13
 */
@Service
public class DataService {

  @Autowired
  private OrderRepo orderRepo;
  @Autowired
  private PVRepo pvRepo;
  @Autowired
  private FavoriteRepo favoriteRepo;
  @Autowired
  private ShoppingCartRepo shoppingCartRepo;


  public Iterable<Order> getOrderByPartition(String partition) {
    Date startDate = DateUtils.getDateBeginTime(partition);
    Date endDate = DateUtils.getDateEndTime(partition);

    return orderRepo.findByCreateTimeAfterAndCreateTimeBefore(startDate, endDate);
  }

  public Iterable<PV> getPVByTypeAndPartition(int type, String partition) {
    Date startDate = DateUtils.getDateBeginTime(partition);
    Date endDate = DateUtils.getDateEndTime(partition);

    return pvRepo.findByTypeAndCreateTimeAfterAndCreateTimeBefore(type, startDate, endDate);
  }

  public Iterable<PV> getPVByPartition(String partition) {
    Date startDate = DateUtils.getDateBeginTime(partition);
    Date endDate = DateUtils.getDateEndTime(partition);

    return pvRepo.findByCreateTimeAfterAndCreateTimeBefore(startDate, endDate);
  }

  public Iterable<Favorite> getFavoriteByPartition(String partition) {
    Date startDate = DateUtils.getDateBeginTime(partition);
    Date endDate = DateUtils.getDateEndTime(partition);

    return favoriteRepo.findByCreateTimeAfterAndCreateTimeBefore(startDate, endDate);
  }

  public Iterable<ShoppingCart> getShoppingByPartition(String partition) {
    Date startDate = DateUtils.getDateBeginTime(partition);
    Date endDate = DateUtils.getDateEndTime(partition);

    return shoppingCartRepo.findByCreateTimeAfterAndCreateTimeBefore(startDate, endDate);
  }


}
