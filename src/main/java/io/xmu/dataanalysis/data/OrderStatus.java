package io.xmu.dataanalysis.data;

import io.xmu.dataanalysis.util.RandomUtils;

public enum OrderStatus {

  NEW(100), PAY(200), SUCCESS_NO_REVIEW(300), SUCCESS_REVIEW(301), SUCCESS_COMPLAINT(302), SUCCESS_RETURN(303);

  private int value;

  OrderStatus (int value) {
    this.value = value;
  }

  public int value() {
    return value;
  }

  public static OrderStatus random() {
    int random = RandomUtils.randomInt(10);

    if (random >= 8) {//20%
      return SUCCESS_REVIEW;
    } else if (random >= 5) {//30%
      return SUCCESS_NO_REVIEW;
    } else if (random >= 4) {//10%
      return SUCCESS_COMPLAINT;
    } else if (random >= 2) {//20%
      return PAY;
    }else if(random>= 1){//10%
      return SUCCESS_RETURN;
    }else {
      return NEW;//10%
    }
  }

  public static boolean isSuccessOrder(int status) {
    return status / 100 == 3;
  }

  public static boolean isCreateOrder(int status) {
    return status == NEW.value || status == PAY.value;
  }
}
