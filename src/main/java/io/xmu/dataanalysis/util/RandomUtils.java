package io.xmu.dataanalysis.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * @author lepdou 2017-02-27
 */
public class RandomUtils {

  private static Random random = new Random();

  public static int randomInt(int size) {
    return random.nextInt(size);
  }

  public static Date randomDate(String dateToDay) {
    Calendar calendar = Calendar.getInstance();

    String[] s = dateToDay.split("-");
    calendar.set(Integer.parseInt(s[0]), Integer.parseInt(s[1])-1, Integer.parseInt(s[2])
        , RandomUtils.randomInt(24), RandomUtils.randomInt(60));

    return calendar.getTime();
  }

}
