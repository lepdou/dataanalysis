package io.xmu.dataanalysis.util;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author apple 16/9/24
 */
public class DateUtils {

    public static Date getCurrentDayBeginTime() {
        return getDayBeginTime(new Date());
    }

    public static Date getDateBeginTime(String partition) {
        String[] s = partition.split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(s[0]));
        calendar.set(Calendar.MONTH, Integer.parseInt(s[1]) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(s[2]));

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new Date(calendar.getTime().getTime());
    }

    public static Date getDateEndTime(String partition) {
        String[] s = partition.split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(s[0]));
        calendar.set(Calendar.MONTH, Integer.parseInt(s[1]) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(s[2]));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return getDateOffset(new Date(calendar.getTime().getTime()), 1);
    }

    private static Date getDayBeginTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new Date(calendar.getTime().getTime());
    }

    public static Date getDateOffset(Date date, int offset) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(calendar.DATE, offset);

        return getDayBeginTime(calendar.getTime());
    }

    public static Date getDateOffset(int offset) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(calendar.DATE, offset);

        return getDayBeginTime(calendar.getTime());
    }

    public static String cutToDay(Date date) {
        SimpleDateFormat dateFormatToDay = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormatToDay.format(date);
    }


    public static String getCurrentYear() {
        SimpleDateFormat dateFormatToYear = new SimpleDateFormat("yyyy");
        return dateFormatToYear.format(new Date());

    }

    public static String getYesterday() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        int dd = calendar.get(Calendar.DATE);
        calendar.set(Calendar.DATE, dd - 1);
        return formatter.format(calendar.getTime());
    }

    public static String getToday() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        return formatter.format(calendar.getTime());
    }

    public static  String getLastWeek(String today){

        try {
            Date date = DateFormat.getDateInstance().parse(today);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE,calendar.get(Calendar.DATE)-7);
            return String.format("%tF",calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getYesterday(String today){
        try {
            Date date = DateFormat.getDateInstance().parse(today);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE,calendar.get(Calendar.DATE)-1);
            return String.format("%tF",calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getWeekDay(String date){
        try {
            Date date1 = DateFormat.getDateInstance().parse(date);


            String weekDay =  String.format("%tA",date1);

            return weekDay.replace("星期","周");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getNextDay(String time){
        try {
            Date date = DateFormat.getDateInstance().parse(time);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE,calendar.get(Calendar.DATE)+1);
            return String.format("%tF",calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getThreeWeekAgo(String today){
        try {
            Date date = DateFormat.getDateInstance().parse(today);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE,calendar.get(Calendar.DATE)-90);
            return String.format("%tF",calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }




    /**
     * 时间是当日
     *
     * @return
     */
    public static boolean isCurrentDay(Date date) {
        return cutToDay(new Date()).equals(cutToDay(date));
    }


    public static void main(String[] args) {
        System.out.println(new Date().getTime());

    }


}
