package io.xmu.dataanalysis.controller;

import com.google.common.collect.Lists;

import io.xmu.dataanalysis.entity.ShopDailyData;
import io.xmu.dataanalysis.repository.GoodsRepo;
import io.xmu.dataanalysis.repository.ShopDailyDataRepo;
import io.xmu.dataanalysis.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

import io.xmu.dataanalysis.entity.Monitor;
import io.xmu.dataanalysis.entity.Shop;
import io.xmu.dataanalysis.model.ShopMonitorModel;
import io.xmu.dataanalysis.model.ShopMonitorVO;
import io.xmu.dataanalysis.repository.MonitorRepo;
import io.xmu.dataanalysis.repository.ShopRepo;

import javax.websocket.server.PathParam;

/**
 * @author lepdou 2017-04-16
 */
@RestController
public class MonitorController {

    @Autowired
    private MonitorRepo monitorRepo;
    @Autowired
    private ShopRepo shopRepo;
    @Autowired
    private GoodsRepo goodsRepo;
    @Autowired
    private ShopDailyDataRepo shopDailyDataRepo;

    @RequestMapping(value = "/monitors", method = RequestMethod.POST)
    public Monitor save(@RequestParam int mainShopId, @RequestBody ShopMonitorModel model) {
        Shop monitorShop = shopRepo.findByName(model.getMonitorShopName());

        if (monitorShop == null) {
            return null;
        }
        Monitor monitor = monitorRepo.findByMainShopIdAndMonitorShopId(mainShopId,monitorShop.getId());
        if(monitor==null) {
            monitor = new Monitor();
            monitor.setMainShopId(mainShopId);
            monitor.setMonitorShopId(monitorShop.getId());
        }
        return monitorRepo.save(monitor);
    }

    @RequestMapping(value = "/monitors/{mainShopId}", method = RequestMethod.GET)
    public List<ShopMonitorVO> getShopMonitor(@PathVariable(value = "mainShopId") int mainShopId) {

        List<Monitor> monitorShops = monitorRepo.findByMainShopId(mainShopId);
        if (CollectionUtils.isEmpty(monitorShops)) {
            return Collections.emptyList();
        }

        List<ShopMonitorVO> result = Lists.newArrayList();
        for (Monitor monitor : monitorShops) {
            result.add(generateMonitorModel(monitor));
        }

        return result;
    }




    // TODO: 17/4/16 渲染一个商户的监控信息
    private ShopMonitorVO generateMonitorModel(Monitor monitorShop) {

        ShopMonitorVO  shopMonitorVO = new ShopMonitorVO();

        Shop shop = shopRepo.findOne(monitorShop.getMonitorShopId());
        shopMonitorVO.setShopName(shop.getName());
        shopMonitorVO.setShopId(shop.getId());

        ShopDailyData today = shopDailyDataRepo.findByShopIdAndPartition(shop.getId(),DateUtils.getToday());
        ShopDailyData yesterday = shopDailyDataRepo.findByShopIdAndPartition(shop.getId(),DateUtils.getYesterday());


        ShopMonitorVO.Counter counter = new ShopMonitorVO.Counter();
        Double data = today.getPayedGoodsCount()*1.0;
        Double data1 = yesterday.getPayedGoodsCount()*1.0;
        Double scale = (data-data1)/data1;
        counter.setCount(data);
        counter.setScale(Math.abs(scale));
        counter.setUp(scale>0);
        shopMonitorVO.setSaleNum(counter);


        counter = new ShopMonitorVO.Counter();
        data = yesterday.getSales()*1.0;
        counter.setCount(data);
        shopMonitorVO.setySales(counter);


        counter = new ShopMonitorVO.Counter();
        data = today.getSales()*1.0;
        data1 = yesterday.getSales()*1.0;
        scale = (data-data1)/data1;
        counter.setCount(data);
        counter.setScale(Math.abs(scale));
        counter.setUp(scale>0);
        shopMonitorVO.setSaleMoney(counter);

        shopMonitorVO.setWords(counter);

        shopMonitorVO.setTofu(counter);

        shopMonitorVO.setTop3Goods(goodsRepo.findByShopId(shop.getId()));

        return shopMonitorVO;
    }


}
