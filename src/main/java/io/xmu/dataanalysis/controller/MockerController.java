package io.xmu.dataanalysis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.xmu.dataanalysis.data.dynamic.FavoriteMocker;
import io.xmu.dataanalysis.data.dynamic.OrderMocker;
import io.xmu.dataanalysis.data.dynamic.PVMocker;
import io.xmu.dataanalysis.data.dynamic.SearchMocker;
import io.xmu.dataanalysis.data.dynamic.ShoppingCartMocker;
import io.xmu.dataanalysis.model.MockModel;
import io.xmu.dataanalysis.repository.CateDailyDataRepo;
import io.xmu.dataanalysis.repository.FavoriteRepo;
import io.xmu.dataanalysis.repository.GoodsDailyDataRepo;
import io.xmu.dataanalysis.repository.OrderRepo;
import io.xmu.dataanalysis.repository.PVRepo;
import io.xmu.dataanalysis.repository.SearchRepo;
import io.xmu.dataanalysis.repository.ShopDailyDataRepo;
import io.xmu.dataanalysis.repository.ShoppingCartRepo;

/**
 * @author lepdou 2017-02-27
 */
@RestController
public class MockerController {

  @Autowired
  private PVMocker pvMocker;
  @Autowired
  private OrderMocker orderMocker;
  @Autowired
  private FavoriteMocker favoriteMocker;
  @Autowired
  private ShoppingCartMocker shoppingCartMocker;
  @Autowired
  private SearchMocker searchMocker;
  @Autowired
  private OrderRepo orderRepo;
  @Autowired
  private CateDailyDataRepo cateDailyDataRepo;
  @Autowired
  private FavoriteRepo favoriteRepo;
  @Autowired
  private GoodsDailyDataRepo goodsDailyDataRepo;
  @Autowired
  private PVRepo pvRepo;
  @Autowired
  private SearchRepo searchRepo;
  @Autowired
  private ShopDailyDataRepo shopDailyDataRepo;
  @Autowired
  private ShoppingCartRepo shoppingCartRepo;

  @Transactional
  @RequestMapping(value = "/data/mock", method = RequestMethod.POST)
  public void mock(@RequestBody MockModel mockModel) {
    String partition = mockModel.getPartition();
    checkDate(mockModel.getPartition());

    pvMocker.mock(partition, mockModel.getPvSize());
    orderMocker.mock(partition, mockModel.getOrderSize());
    favoriteMocker.mock(partition, mockModel.getFavoriteSize());
    searchMocker.mock(partition, mockModel.getSearchSize());
    shoppingCartMocker.mock(partition, mockModel.getShoppingCartSize());

  }

  @Transactional
  @RequestMapping(value = "/data/clear", method = RequestMethod.DELETE)
  public void delete(@RequestParam(required = false) String partition) {
    orderRepo.deleteAll();
    favoriteRepo.deleteAll();
    searchRepo.deleteAll();
    shoppingCartRepo.deleteAll();
    pvRepo.deleteAll();

    shopDailyDataRepo.deleteAll();
    goodsDailyDataRepo.deleteAll();
    cateDailyDataRepo.deleteAll();
  }

  @RequestMapping(value = "/pvs/mock", method = RequestMethod.POST)
  public void mockPV(@RequestParam(defaultValue = "10") int size, @RequestParam String date) {
    checkDate(date);

    pvMocker.mock(date, size);
  }

  @RequestMapping(value = "/orders/mock", method = RequestMethod.POST)
  public void mockOrder(@RequestParam(defaultValue = "10") int size, @RequestParam String date) {
    checkDate(date);
    orderMocker.mock(date, size);
  }

  @RequestMapping(value = "/favorites/mock", method = RequestMethod.POST)
  public void mockFavorite(@RequestParam(defaultValue = "10") int size, @RequestParam String date) {
    checkDate(date);
    favoriteMocker.mock(date, size);
  }

  @RequestMapping(value = "/shoppingcart/mock", method = RequestMethod.POST)
  public void mockShoppingCart(@RequestParam(defaultValue = "10") int size, @RequestParam String date) {
    checkDate(date);
    shoppingCartMocker.mock(date, size);
  }

  @RequestMapping(value = "/search/mock", method = RequestMethod.POST)
  public void mockSearch(@RequestParam(defaultValue = "10") int size, @RequestParam String date) {
    checkDate(date);
    searchMocker.mock(date, size);
  }


  //date 格式必须是: yyyy-mm-dd
  private void checkDate(String dateToDay) {
    if (!dateToDay.contains("-") || dateToDay.split("-").length != 3){
      throw new IllegalArgumentException();
    }

  }

}
