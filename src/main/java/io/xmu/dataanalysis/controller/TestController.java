package io.xmu.dataanalysis.controller;

import io.xmu.dataanalysis.entity.GoodsDailyData;
import io.xmu.dataanalysis.entity.Order;
import io.xmu.dataanalysis.entity.ShopDailyData;
import io.xmu.dataanalysis.repository.*;
import io.xmu.dataanalysis.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Created by Jim Chen at XMU on 2017/4/5.
 */

@RestController
public class TestController {

    @Autowired
    ShopDailyDataRepo shopDailyDataRepo;

    @Autowired
    OrderRepo orderRepo;

    @Autowired
    GoodsDailyDataRepo goodsDailyDataRepo;

    @Autowired
    SearchRepo searchRepo;
    @Autowired
    UserRepo userRepo;


    @RequestMapping(value = "test/shop", method = RequestMethod.GET)
    public Iterable<ShopDailyData> testShopDailyData() {

        return shopDailyDataRepo.findAll();
    }

    @RequestMapping(value = "test/shop/one", method = RequestMethod.GET)
    public ShopDailyData testShopDailyData1(@RequestParam(value = "shop_id", defaultValue = "0") Integer shopId,
                                            @RequestParam(value = "time", defaultValue = "0") String time) {
        ShopDailyData shopDailyData = shopDailyDataRepo.findByShopIdAndPartition(shopId, time);
        return shopDailyData;
    }

    @RequestMapping(value = "test/shop/average-pv", method = RequestMethod.GET)
    public List<ShopDailyData> testShopDailyData2(@RequestParam(value = "shop_id", defaultValue = "0") Integer shopId,
                                                 @RequestParam(value = "start", defaultValue = "0") String start,
                                                 @RequestParam(value = "end", defaultValue = "0") String end
    ) {
        return  shopDailyDataRepo.findByShopIdAndPartitionBetween(shopId, start, end);
    }

    @RequestMapping(value = "test/order",method = RequestMethod.GET)
    public List<Order> getOrdersByShopAndTime(@RequestParam(value = "shop_id",defaultValue = "0")Integer shopId,
                                              @RequestParam(value = "start", defaultValue = "0") String start,
                                              @RequestParam(value = "end", defaultValue = "0") String end){
        return orderRepo.findByShopIdAndCreateTime(shopId,start,end);
    }


    @RequestMapping(value = "test/good-top10/")
    public List<GoodsDailyData> getTop10Good(@RequestParam(value = "time",defaultValue = "")String time){
        return goodsDailyDataRepo.findTop10GoodsPVByPartition(time);
    }

    @RequestMapping(value = "test/Sales-top10",method = RequestMethod.GET)
    public List<ShopDailyData> getSalesTop10(@RequestParam(value = "time",defaultValue = "")String time){
        return shopDailyDataRepo.findSalesTop10ByPartition(time);
    }


    @RequestMapping(value = "/test/keyword",method = RequestMethod.GET)
    public List<Object[]> getKeyword(@RequestParam(value = "time",defaultValue = "")String time){
        if (time.equals(""))time=String.format("%tF",new Date());

        return searchRepo.findSearchKeywordTop10(time, DateUtils.getNextDay(time));
    }

    @RequestMapping(value = "/test/averageSales",method = RequestMethod.GET)
    public List<BigDecimal> getAverageSales(){
        return shopDailyDataRepo.findAverageSales(100);
    }

    @RequestMapping(value = "/test/myCustomersss",method = RequestMethod.GET)
    public List<Object[]> getMyCustomers(Integer shopId) {

        List<Object[]> goods =  userRepo.findGoodsByIdAndShopIdAndTime(971,100,"2017-02-07","2017-05-07");
        System.out.print(goods.get(0)[1].getClass());
        System.out.print(goods.get(0)[0].getClass());
        return goods;
    }

}
