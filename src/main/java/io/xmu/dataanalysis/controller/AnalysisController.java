package io.xmu.dataanalysis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.xmu.dataanalysis.data.analysis.CateDataAnalysis;
import io.xmu.dataanalysis.data.analysis.GoodsDataAnalysis;
import io.xmu.dataanalysis.data.analysis.ShopDataAnalysis;

/**
 * @author lepdou 2017-03-06
 */
@RestController
public class AnalysisController {

  @Autowired
  private ShopDataAnalysis shopDataAnalysis;
  @Autowired
  private GoodsDataAnalysis goodsDataAnalysis;
  @Autowired
  private CateDataAnalysis cateDataAnalysis;

  @RequestMapping(value = "/data/analysis", method = RequestMethod.POST)
  public void analysis(@RequestParam String partition) {
    shopDataAnalysis.analysis(partition);
    goodsDataAnalysis.analysis(partition);
    cateDataAnalysis.analysis(partition);
  }

  @RequestMapping(value = "/shops/analysis", method = RequestMethod.POST)
  public void analysisShopData(@RequestParam String partition) {
    shopDataAnalysis.analysis(partition);
  }

  @RequestMapping(value = "/goods/analysis", method = RequestMethod.POST)
  public void analysisGoodsData(@RequestParam String partition) {
    goodsDataAnalysis.analysis(partition);
  }

  @RequestMapping(value = "/cates/analysis", method = RequestMethod.POST)
  public void analysisCatesData(@RequestParam String partition) {
    cateDataAnalysis.analysis(partition);
  }

}
