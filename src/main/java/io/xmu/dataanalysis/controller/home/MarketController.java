package io.xmu.dataanalysis.controller.home;

import io.xmu.dataanalysis.service.home.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/4/7.
 */

@RestController
public class MarketController {


    private MarketService marketService;

    @Autowired
    public MarketController(MarketService marketService) {
        this.marketService = marketService;
    }

    @RequestMapping(value = "/api/home/market/good-view-top10",method = RequestMethod.GET)
    public List<Map<String,Object>> getGoodsViewTop10(@RequestParam(value = "time",defaultValue = "") String time) {

        if (time.equals(""))time = String.format("%tF",new Date());
        return marketService.getGoodViewTop10(time);
    }

    @RequestMapping(value = "/api/home/market/search-keyword-top10",method = RequestMethod.GET)
    public List<Map<String,Object>> getSearchKeywordTop10(@RequestParam(value = "time",defaultValue = "") String time){
        if(time.equals(""))time = String.format("%tF",new Date());
        return marketService.getSearchKeywordTop10(time);
    }

    @RequestMapping(value = "/api/home/market/sales-top10",method = RequestMethod.GET)
    public List<Map<String,String>> getSalesTop10(@RequestParam(value = "time",defaultValue = "")String time) {
        if(time.equals(""))time = String.format("%tF",new Date());
        return marketService.getSalesTop10(time);
    }

    @RequestMapping(value = "/api/home/market/shop-flow-top10",method = RequestMethod.GET)
    public List<Map<String,Object>> getShopFlowTop10(@RequestParam(value = "time",defaultValue = "")String time){
        if(time.equals(""))time = String.format("%tF",new Date());
        return marketService.getShopFlowTop10(time);
    }

    @RequestMapping(value = "/api/home/market/good-trade-top10",method = RequestMethod.GET)
    public List<Map<String,Object>> getGoodTradeTop10(@RequestParam(value = "time",defaultValue = "")String time){
        if(time.equals(""))time = String.format("%tF",new Date());
        return marketService.getGoodTradeTop10(time);
    }

}
