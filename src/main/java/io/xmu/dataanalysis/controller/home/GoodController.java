package io.xmu.dataanalysis.controller.home;

import io.xmu.dataanalysis.service.home.GoodService;
import io.xmu.dataanalysis.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/4/11.
 */

@RestController
public class GoodController {

    private GoodService goodService;

    @Autowired
    public GoodController(GoodService goodService) {
        this.goodService = goodService;
    }

    @RequestMapping(value = "/api/home/good", method = RequestMethod.GET)
    public Map<String, Double[]> getGood(@RequestParam(value = "shop_id", defaultValue = "0") Integer shopId) {
        if (shopId == 0) return null;
        return goodService.getGood(shopId);
    }
    
}
