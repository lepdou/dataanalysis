package io.xmu.dataanalysis.controller.home;

import io.xmu.dataanalysis.service.home.HomeService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/4/4.
 */

@RestController
public class HomeController2 {


    private HomeService2 homeService;

    @Autowired
    public HomeController2(HomeService2 homeService) {
        this.homeService = homeService;
    }

    //只选了本周的待办事项，以为待办事项太多了
    @RequestMapping(value = "/api/home/backlog",method = RequestMethod.GET)
    public Map<String, Integer> getBackLog(@RequestParam(value = "shop_id", defaultValue = "0") Integer shopId,
                                    @RequestParam(value = "time", defaultValue = "") String time) {
        if (shopId==0) return null;
        if (time.equals("")) time = String.format("%tF", new Date());
        return homeService.getBackLog(shopId, time);
    }


    @RequestMapping(value = "/api/home/indices",method = RequestMethod.GET)
    public List<Map<String,Double>> getIndices(@RequestParam(value = "shop_id",defaultValue = "0")Integer shopId,
                                               @RequestParam(value = "time",defaultValue = "")String time){
        if (shopId==0)return null;
        if(time.equals(""))time = String.format("%tF",new Date());
        return homeService.getIndices(shopId, time);
    }

    @RequestMapping(value = "/api/home/payed-user-count")
    public List<Object[]> getPayedUserCount(@RequestParam(value = "shop_id",defaultValue = "0") Integer shopId){
        if (shopId==0)return null;
        return homeService.getPayedUserCount(shopId);
    }

    @RequestMapping(value = "/api/home/payed-goods-count")
    public List<Object[]> getPayedGoodsCount(@RequestParam(value = "shop_id",defaultValue = "0") Integer shopId){
        if (shopId==0)return null;
        return homeService.getPayedGoodsCount(shopId);
    }

    @RequestMapping(value = "/api/home/sales")
    public List<Object[]> getSales(@RequestParam(value = "shop_id",defaultValue = "0") Integer shopId){
        if (shopId==0)return null;
        return homeService.getSales(shopId);
    }

    @RequestMapping(value = "/api/home/pv")
    public List<Object[]> getPV(@RequestParam(value = "shop_id",defaultValue = "0") Integer shopId){
        if (shopId==0)return null;
        return homeService.getPV(shopId);
    }

    @RequestMapping(value = "/api/home/uv")
    public List<Object[]> getUV(@RequestParam(value = "shop_id",defaultValue = "0") Integer shopId){
        if (shopId==0)return null;
        return homeService.getUV(shopId);
    }

    @RequestMapping(value = "/api/home/summery",method = RequestMethod.GET)
    public List<String> getSummery(@RequestParam(value = "shop_id",defaultValue = "0")Integer shopId){
        return homeService.getSummery(shopId);
    }

    @RequestMapping(value = "/api/home/customer",method = RequestMethod.GET)
    public List<Map<String,Object>> getCustomerClassification(@RequestParam(value = "shop_id",defaultValue = "0")Integer shopId){
        return homeService.getCustomerClassification(shopId);
    }



}
