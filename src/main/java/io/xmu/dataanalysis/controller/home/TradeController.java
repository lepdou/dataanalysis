package io.xmu.dataanalysis.controller.home;

import io.xmu.dataanalysis.service.home.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/4/6.
 */
@RestController
public class TradeController {

    private TradeService tradeService;

    @Autowired
    TradeController(TradeService tradeService) {
        this.tradeService = tradeService;
    }


    @RequestMapping(value = "/api/home/trade",method = RequestMethod.GET)
    public Map<String,Double[]> getTrade(@RequestParam(value = "shop_id",defaultValue = "0")Integer shopId,
                                         @RequestParam(value = "time",defaultValue = "")String time){
        if(shopId==0)return null;
        if(time.equals(""))time = String.format("%tF",new Date());
        return tradeService.getTrade(shopId, time);
    }

    @RequestMapping(value = "/api/home/trade/order-user-count")
    public List<Object[]> getOrderUserCount(@RequestParam(value = "shop_id",defaultValue = "0")Integer shopId,
                                             @RequestParam(value = "time",defaultValue = "")String time){
        if(shopId==0)return null;
        if(time.equals(""))time = String.format("%tF",new Date());
        return  tradeService.getOrderUserCount(shopId, time);
    }

    @RequestMapping(value = "/api/home/trade/paid-user-count")
    public List<Object[]> getPaidUserCount(@RequestParam(value = "shop_id",defaultValue = "0")Integer shopId,
                                            @RequestParam(value = "time",defaultValue = "")String time){
        if(shopId==0)return null;
        if(time.equals(""))time = String.format("%tF",new Date());
        return tradeService.getPaidUserCount(shopId, time);
    }

    @RequestMapping(value = "/api/home/trade/paid-good-count")
    public List<Object[]> getPaidGoodCount(@RequestParam(value = "shop_id",defaultValue = "0")Integer shopId,
                                            @RequestParam(value = "time",defaultValue = "")String time){
        if(shopId==0)return null;
        if(time.equals(""))time = String.format("%tF",new Date());
        return  tradeService.getPaidGoodCount(shopId, time);
    }

}
