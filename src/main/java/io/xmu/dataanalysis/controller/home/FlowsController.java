package io.xmu.dataanalysis.controller.home;

import io.xmu.dataanalysis.service.home.FlowsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/4/4.
 */

@RestController
public class FlowsController {
    private FlowsService flowsService;

    @Autowired
    public FlowsController(FlowsService flowsService) {
        this.flowsService = flowsService;
    }

    @RequestMapping(value = "/api/home/flows", method = RequestMethod.GET)
    public Map<String, Double[]> getFlows(@RequestParam(value = "shop_id", defaultValue = "0") Integer shopId,
                                   @RequestParam(value = "time", defaultValue = "") String time) {
        if (shopId == 0) return null;
        if (time.equals("")) time = String.format("%tF", new Date());
        return flowsService.getFlows(shopId, time);
    }

    @RequestMapping(value = "/api/home/flows/bounce-rate", method = RequestMethod.GET)
    public List<Object[]> getBounceRate(@RequestParam(value = "shop_id", defaultValue = "0") Integer shopId,
                             @RequestParam(value = "time", defaultValue = "") String time) {
        if (shopId == 0) return null;
        if (time.equals("")) time = String.format("%tF", new Date());
        return flowsService.getBounceRate(shopId, time);
    }

    @RequestMapping(value = "/api/home/flows/average-stay-time", method = RequestMethod.GET)
    public List<Object[]> getAverageStayTime(@RequestParam(value = "shop_id", defaultValue = "0") Integer shopId,
                                  @RequestParam(value = "time", defaultValue = "") String time) {
        if (shopId == 0) return null;
        if (time.equals("")) time = String.format("%tF", new Date());
        return flowsService.getAverageStayTime(shopId, time);
    }

        @RequestMapping(value = "/api/home/flows/average-pv", method = RequestMethod.GET)
    public List<Object[]> getAveragePV(@RequestParam(value = "shop_id", defaultValue = "0") Integer shopId,
                                @RequestParam(value = "time", defaultValue = "") String time) {
        if (shopId == 0) return null;
        if (time.equals("")) time = String.format("%tF", new Date());
        return flowsService.getAveragePV(shopId, time);
    }

}
