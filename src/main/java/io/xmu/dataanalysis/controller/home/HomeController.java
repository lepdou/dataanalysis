package io.xmu.dataanalysis.controller.home;

import io.xmu.dataanalysis.service.home.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/3/4.
 */
@RestController
public class HomeController {

    @Autowired
    HomeService homeService;


    @RequestMapping(value = "/api/home/indices",method = RequestMethod.POST)
    public Map<String,Object> getIndices(@RequestParam(value = "shop_id") Integer shopId){
        return homeService.getDailyData(shopId);
    }

}
