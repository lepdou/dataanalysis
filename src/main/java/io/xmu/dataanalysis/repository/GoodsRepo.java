package io.xmu.dataanalysis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import io.xmu.dataanalysis.entity.Goods;

import java.util.List;

public interface GoodsRepo extends PagingAndSortingRepository<Goods, Integer> {

  Goods findByNameAndShopId(String name, int shopId);


  @Query(value = "SELECT `Pic` FROM goods WHERE `ShopId` =?1 ORDER BY `Price` DESC LIMIT 3 ",nativeQuery = true)
  List<String> findByShopId(Integer shopId);

  @Query(value ="SELECT price FROM goods where id = ?1" ,nativeQuery = true)
  Double findPriceById(Integer id);

}
