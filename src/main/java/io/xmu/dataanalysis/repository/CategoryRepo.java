package io.xmu.dataanalysis.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.xmu.dataanalysis.entity.Category;

public interface CategoryRepo extends PagingAndSortingRepository<Category, Integer> {

  Category findByName(String name);

}
