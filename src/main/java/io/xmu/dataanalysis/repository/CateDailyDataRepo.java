package io.xmu.dataanalysis.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import io.xmu.dataanalysis.entity.CateDailyData;

public interface CateDailyDataRepo extends PagingAndSortingRepository<CateDailyData, Integer> {

  @Modifying
  @Query("delete from CateDailyData where partition = ?1")
  void deleteByPartition(String partition);

  CateDailyData findByCateIdAndPartition(int cateId, String partition);
}
