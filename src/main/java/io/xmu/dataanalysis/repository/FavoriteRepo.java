package io.xmu.dataanalysis.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

import io.xmu.dataanalysis.entity.Favorite;

/**
 * @author lepdou 2017-02-25
 */
public interface FavoriteRepo extends PagingAndSortingRepository<Favorite, Integer> {


  List<Favorite> findByCreateTimeAfterAndCreateTimeBefore(Date startTime, Date endTime);

}
