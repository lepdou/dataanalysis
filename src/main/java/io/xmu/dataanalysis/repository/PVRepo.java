package io.xmu.dataanalysis.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

import io.xmu.dataanalysis.entity.PV;

public interface PVRepo extends PagingAndSortingRepository<PV, Integer> {

  List<PV> findByTypeAndCreateTimeAfterAndCreateTimeBefore(int type, Date startTime, Date endTime);

  List<PV> findByCreateTimeAfterAndCreateTimeBefore(Date startTime, Date endTime);

}
