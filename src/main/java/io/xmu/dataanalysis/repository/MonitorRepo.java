package io.xmu.dataanalysis.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

import io.xmu.dataanalysis.entity.Monitor;

public interface MonitorRepo extends PagingAndSortingRepository<Monitor, Integer> {

    List<Monitor> findByMainShopId(int mainShopId);

    Monitor findByMainShopIdAndMonitorShopId(Integer mainShopId,Integer monitorShopId);

}
