package io.xmu.dataanalysis.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.xmu.dataanalysis.entity.Shop;

public interface ShopRepo extends PagingAndSortingRepository<Shop, Integer> {

  Shop findByName(String name);

}
