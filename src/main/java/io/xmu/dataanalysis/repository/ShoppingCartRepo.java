package io.xmu.dataanalysis.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

import io.xmu.dataanalysis.entity.ShoppingCart;

public interface ShoppingCartRepo extends PagingAndSortingRepository<ShoppingCart, Integer> {


  List<ShoppingCart> findByCreateTimeAfterAndCreateTimeBefore(Date startTime, Date endTime);

}
