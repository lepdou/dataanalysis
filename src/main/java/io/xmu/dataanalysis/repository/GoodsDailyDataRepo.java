package io.xmu.dataanalysis.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import io.xmu.dataanalysis.entity.GoodsDailyData;

import java.util.List;

public interface GoodsDailyDataRepo extends PagingAndSortingRepository<GoodsDailyData, Integer> {

    @Modifying
    @Query("delete from GoodsDailyData where partition = ?1")
    void deleteByPartition(String partition);

    GoodsDailyData findByGoodsIdAndPartition(int goodsId, String partition);

    @Query(value = "SELECT * FROM goodsdailydata  " +
            " WHERE `Partition` = ?1 " +
            " ORDER BY `PV` DESC LIMIT 10 ",nativeQuery = true)
    List<GoodsDailyData> findTop10GoodsPVByPartition(String partition);

    @Query(value = " SELECT * FROM goodsdailydata WHERE `Partition` = ?1 " +
            " ORDER BY PayMoneyCount DESC LIMIT 10 ",nativeQuery = true)
    List<GoodsDailyData> findTop10GoodsSalesByPartition(String partition);



}
