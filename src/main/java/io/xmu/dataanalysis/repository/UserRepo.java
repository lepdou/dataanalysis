package io.xmu.dataanalysis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import io.xmu.dataanalysis.entity.User;

import java.util.List;

public interface UserRepo extends PagingAndSortingRepository<User, Integer> {

  User findByName(String name);


  @Query(value = "SELECT `GoodsId`,`CreateTime` FROM `order`\n" +
          "WHERE \n" +
          "UserId = ?1 \n" +
          "AND GoodsId IN (\n" +
          "SELECT `Id` FROM `goods` WHERE ShopId = ?2)\n" +
          "AND `Status` > 100\n" +
          "AND `CreateTime` > ?3\n" +
          "AND `CreateTime` < ?4\n" +
          "ORDER BY `CreateTime` DESC",nativeQuery = true)
  List<Object[]> findGoodsByIdAndShopIdAndTime(Integer id,Integer shopId,String from,String to);


}
