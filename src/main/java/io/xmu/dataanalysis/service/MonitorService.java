package io.xmu.dataanalysis.service;

import io.xmu.dataanalysis.repository.MonitorRepo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Jim Chen at XMU on 2017/4/29.
 */
public class MonitorService {

    private MonitorRepo monitorRepo;

    @Autowired
    public MonitorService(MonitorRepo monitorRepo){
        this.monitorRepo = monitorRepo;
    }

}
