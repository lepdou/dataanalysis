package io.xmu.dataanalysis.service.home;

import io.xmu.dataanalysis.entity.ShopDailyData;
import io.xmu.dataanalysis.repository.OrderRepo;
import io.xmu.dataanalysis.repository.ShopDailyDataRepo;
import io.xmu.dataanalysis.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Jim Chen at XMU on 2017/4/6.
 */

@Service
public class TradeService {

    private ShopDailyDataRepo shopDailyDataRepo;

    private OrderRepo orderRepo;

    @Autowired
    public TradeService(ShopDailyDataRepo shopDailyDataRepo,OrderRepo orderRepo) {
        this.shopDailyDataRepo = shopDailyDataRepo;
        this.orderRepo = orderRepo;
    }

    public Map<String, Double[]> getTrade(Integer shopId, String time) {
        ShopDailyData today = shopDailyDataRepo.findByShopIdAndPartition(shopId, time);
        ShopDailyData yesterday = shopDailyDataRepo.findByShopIdAndPartition(shopId, DateUtils.getYesterday(time));
        ShopDailyData lastWeek = shopDailyDataRepo.findByShopIdAndPartition(shopId, DateUtils.getLastWeek(time));

        Double[] orderUserCount = new Double[]{0.0,0.0,0.0};
        String start = time;
        String end = DateUtils.getNextDay(time);
        orderUserCount[0] = 1.0*orderRepo.findOrderUserCountByShopIdAnAndCreateTime(shopId,start,end);
        orderUserCount[1] = 1.0*orderRepo.findOrderUserCountByShopIdAnAndCreateTime(shopId,DateUtils.getYesterday(start),DateUtils.getYesterday(end));
        orderUserCount[2] = 1.0*orderRepo.findOrderUserCountByShopIdAnAndCreateTime(shopId,DateUtils.getLastWeek(start),DateUtils.getLastWeek(end));

        orderUserCount[1] = (orderUserCount[0]-orderUserCount[1])/orderUserCount[1];
        orderUserCount[1] = Double.parseDouble(String.format("%.4f",orderUserCount[1]));

        orderUserCount[2] = (orderUserCount[0]-orderUserCount[2])/orderUserCount[2];
        orderUserCount[2] = Double.parseDouble(String.format("%.4f",orderUserCount[2]));

        Double[] paidUserCount = new Double[]{0.0,0.0,0.0};
        paidUserCount[0] = today.getPayedUserCount()*1.0;

        paidUserCount[1] = yesterday.getPayedUserCount()*1.0;
        paidUserCount[1] = (paidUserCount[0]-paidUserCount[1])/paidUserCount[1];
        paidUserCount[1] = Double.parseDouble(String.format("%.4f",paidUserCount[1]));

        paidUserCount[2] = lastWeek.getPayedUserCount()*1.0;
        paidUserCount[2] = (paidUserCount[0]-paidUserCount[2])/paidUserCount[2];
        paidUserCount[2] = Double.parseDouble(String.format("%.4f",paidUserCount[2]));

        Double[] paidGoodCount = new Double[]{0.0,0.0,0.0};
        paidGoodCount[0] = today.getPayedGoodsCount()*1.0;

        paidGoodCount[1] = yesterday.getPayedGoodsCount()*1.0;
        paidGoodCount[1] = (paidGoodCount[0]-paidGoodCount[1])/paidGoodCount[1];
        paidGoodCount[1] = Double.parseDouble(String.format("%.4f",paidGoodCount[1]));

        paidGoodCount[2] = lastWeek.getPayedGoodsCount()*1.0;
        paidGoodCount[2] = (paidGoodCount[0]-paidGoodCount[2])/paidGoodCount[2];
        paidGoodCount[2] = Double.parseDouble(String.format("%.4f",paidGoodCount[2]));

        Map<String,Double[]> data = new HashMap<>();
        data.put("orderUserCount",orderUserCount);
        data.put("payedUserCount",paidUserCount);
        data.put("payedGoodCount",paidGoodCount);

        return data;
    }

    public List<Object[]> getOrderUserCount(Integer shopId, String time) {
        LinkedList<Object[]> data = new LinkedList<>();
        String start =  time;
        String end = DateUtils.getNextDay(start);

        for(int i=0;i<14;i++){
            Object[] td= new Object[2];
            td[0] = DateUtils.getWeekDay(start);
            td[1] = orderRepo.findOrderUserCountByShopIdAnAndCreateTime(shopId,start,end);
            data.addFirst(td);
            end = start;
            start = DateUtils.getYesterday(start);
        }
        return data;
    }

    public List<Object[]> getPaidUserCount(Integer shopId, String time) {
        LinkedList<Object[]>  data  = new LinkedList<>();
        String twoWeekAgo = DateUtils.getLastWeek(time);
        twoWeekAgo = DateUtils.getLastWeek(twoWeekAgo);

        List<ShopDailyData> dailyData = shopDailyDataRepo.findByShopIdAndPartitionBetween(shopId,twoWeekAgo,time);

        int i=0;
        int size = dailyData.size();

        for(;i<size;i++){
            Object[] td = new Object[2];
            td[0] = DateUtils.getWeekDay(dailyData.get(i).getPartition());
            td[1] = dailyData.get(i).getPayedUserCount();
            data.addFirst(td);
        }

        int sub = 14-i;
        String date = dailyData.get(i-1).getPartition();
        date = DateUtils.getYesterday(date);
        while(sub-->0){
            data.addFirst(new Object[]{date,0.0});
            date = DateUtils.getYesterday(date);
        }

        return data;
    }

    public List<Object[]> getPaidGoodCount(Integer shopId, String time) {
        LinkedList<Object[]>  data  = new LinkedList<>();

        String twoWeekAgo = DateUtils.getLastWeek(time);
        twoWeekAgo = DateUtils.getLastWeek(twoWeekAgo);

        List<ShopDailyData> dailyData = shopDailyDataRepo.findByShopIdAndPartitionBetween(shopId,twoWeekAgo,time);

        int i=0;
        int size = dailyData.size();

        for(;i<size;i++){
            Object[] td = new Object[2];
            td[0] = DateUtils.getWeekDay(dailyData.get(i).getPartition());
            td[1] = dailyData.get(i).getPayedGoodsCount();
            data.addFirst(td);
        }

        int sub = 14-i;
        String date = dailyData.get(i-1).getPartition();
        date = DateUtils.getYesterday(date);
        while(sub-->0){
            data.addFirst(new Object[]{date,0.0});
            date = DateUtils.getYesterday(date);
        }

        return data;

    }


}
