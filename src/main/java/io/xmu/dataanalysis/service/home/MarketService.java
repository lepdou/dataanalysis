package io.xmu.dataanalysis.service.home;

import io.xmu.dataanalysis.entity.Goods;
import io.xmu.dataanalysis.entity.GoodsDailyData;
import io.xmu.dataanalysis.entity.Shop;
import io.xmu.dataanalysis.entity.ShopDailyData;
import io.xmu.dataanalysis.repository.*;
import io.xmu.dataanalysis.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/4/7.
 */
@Service
public class MarketService {

    private GoodsDailyDataRepo goodsDailyDataRepo;

    private GoodsRepo goodsRepo;

    private SearchRepo searchRepo;

    private ShopDailyDataRepo shopDailyDataRepo;

    private ShopRepo shopRepo;

    @Autowired
    public MarketService(GoodsDailyDataRepo goodsDailyDataRepo,
                         GoodsRepo goodsRepo,
                         SearchRepo searchRepo,
                         ShopDailyDataRepo shopDailyDataRepo,
                         ShopRepo shopRepo){
        this.goodsDailyDataRepo = goodsDailyDataRepo;
        this.goodsRepo = goodsRepo;
        this.searchRepo = searchRepo;
        this.shopDailyDataRepo = shopDailyDataRepo;
        this.shopRepo=shopRepo;
    }


    public List<Map<String,Object>> getGoodViewTop10(String time){
        List<Map<String,Object>>  list = new ArrayList<>();
        List<GoodsDailyData> top10Goods = goodsDailyDataRepo.findTop10GoodsPVByPartition(time);
        for(GoodsDailyData dailyData: top10Goods){
            Integer goodId = dailyData.getGoodsId();
            Goods goods = goodsRepo.findOne(goodId);
            Map<String,Object> map = new HashMap<>();
            map.put("imgUrl",goods.getPic());
            map.put("name",goods.getName());
            map.put("pv",dailyData.getPv());
            list.add(map);
        }
        return list;
    }

    public List<Map<String,Object>> getSearchKeywordTop10(String time){
        List<Map<String,Object>> list = new ArrayList<>();
        String nextDay = DateUtils.getNextDay(time);
        String lastDay = DateUtils.getYesterday(time);
        List<Object[]> data = searchRepo.findSearchKeywordTop10(time,nextDay);
        for(Object[] objects:data){
            Integer count =searchRepo.findCountByKeyAndModifiedTime((String)objects[0],lastDay,time);
            Map<String,Object> map = new HashMap<>();
            map.put("keyword",objects[0]);
            map.put("index",((BigInteger)objects[1]).longValue()*12.5);
            map.put("increment", (((BigInteger)objects[1]).longValue()-count)*1.0/count);
            list.add(map);
        }
        return list;
    }

    public List<Map<String,String>> getSalesTop10(String time){
        List<ShopDailyData> data = shopDailyDataRepo.findSalesTop10ByPartition(time);
        List<Map<String,String>> sales = new ArrayList<>();
        for(ShopDailyData dailyData:data){
            Integer id = dailyData.getShopId();
            Shop shop = shopRepo.findOne(id);
            Map<String,String> map = new HashMap<>();
            map.put("logUrl",shop.getPic());
            map.put("shopName",shop.getName());
            sales.add(map);
        }
        return sales;
    }

    public List<Map<String,Object>> getGoodTradeTop10(String time){
        List<Map<String,Object>> list = new ArrayList<>();
        List<GoodsDailyData> goodsDailyData = goodsDailyDataRepo.findTop10GoodsSalesByPartition(time);
        for(GoodsDailyData dailyData:goodsDailyData){
            Integer id = dailyData.getGoodsId();
            Goods goods = goodsRepo.findOne(id);
            Map<String,Object> map = new HashMap<>();
            map.put("imgUrl", goods.getPic());
            map.put("name",goods.getName());
            map.put("tradeIndex", dailyData.getPayMoneyCount()/10);
            list.add(map);
        }
        return list;
    }

    public List<Map<String,Object>> getShopFlowTop10(String time) {
        List<Map<String,Object>> list = new ArrayList<>();
        List<ShopDailyData> dailyData = shopDailyDataRepo.findPVTop10ByPartition(time);
        for (ShopDailyData shopDailyData:dailyData){
            Integer shopId=shopDailyData.getShopId();
            Shop shop = shopRepo.findOne(shopId);
            Map<String,Object> map = new HashMap<>();
            map.put("logUrl",shop.getPic());
            map.put("shopName",shop.getName());
            map.put("flowIndex",shopDailyData.getPv()*10);
            list.add(map);
        }

        return list;
    }


}
