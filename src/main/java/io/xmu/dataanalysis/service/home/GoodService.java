package io.xmu.dataanalysis.service.home;

import io.xmu.dataanalysis.entity.ShopDailyData;
import io.xmu.dataanalysis.repository.GoodsRepo;
import io.xmu.dataanalysis.repository.ShopDailyDataRepo;
import io.xmu.dataanalysis.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/4/11.
 */

@Service
public class GoodService {

    private GoodsRepo goodsRepo;
    private ShopDailyDataRepo shopDailyDataRepo;

    @Autowired
    public GoodService(GoodsRepo goodsRepo, ShopDailyDataRepo shopDailyDataRepo) {
        this.goodsRepo = goodsRepo;
        this.shopDailyDataRepo = shopDailyDataRepo;
    }

    public Map<String, Double[]> getGood(Integer shopId) {

        Map<String, Double[]> goods = new HashMap<>();
        String time = DateUtils.getToday();
        String yTime  = DateUtils.getYesterday();
        String lTime = DateUtils.getLastWeek(time);

        ShopDailyData today = shopDailyDataRepo.findByShopIdAndPartition(shopId, time);
        ShopDailyData yesterDay = shopDailyDataRepo.findByShopIdAndPartition(shopId, yTime);
        ShopDailyData lastWeek = shopDailyDataRepo.findByShopIdAndPartition(shopId, lTime);
        double tmp = today.getAddShoppingCartCount();
        double tmp1 = yesterDay.getAddShoppingCartCount();
        double tmp2 = lastWeek.getAddShoppingCartCount();

        goods.put("moreGood", new Double[]{tmp,
                (tmp - tmp1) / tmp1,
                (tmp - tmp2) / tmp2});

        tmp = shopDailyDataRepo.findFavoriteCountByShopIdAndPartition(shopId,time);
        tmp1 = shopDailyDataRepo.findFavoriteCountByShopIdAndPartition(shopId,yTime);
        tmp2 = shopDailyDataRepo.findFavoriteCountByShopIdAndPartition(shopId,lTime);
        goods.put("favorites", new Double[]{tmp,
                (tmp - tmp1) / tmp1,
                (tmp - tmp2) / tmp2});

        goods.put("averageBounceRate", new Double[]{0.3456,
                0.1632,
                0.051});

        tmp = today.getViewGoodsCount() * 1.0;
        tmp1 = yesterDay.getViewGoodsCount() * 1.0;
        goods.put("goodView", new Double[]{tmp,
                (tmp - tmp1) / tmp1});

        tmp = today.getCreateOrderCount();
        tmp1 = yesterDay.getCreateOrderCount();
        goods.put("orderGood", new Double[]{tmp,
                (tmp - tmp1) / tmp1});

        tmp = today.getPayedGoodsCount();
        tmp1 = yesterDay.getPayedGoodsCount();
        goods.put("payedGood", new Double[]{tmp,
                (tmp - tmp1) / tmp1});

        return goods;
    }

}
