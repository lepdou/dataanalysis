package io.xmu.dataanalysis.service.home;

import io.xmu.dataanalysis.entity.Order;
import io.xmu.dataanalysis.entity.ShopDailyData;
import io.xmu.dataanalysis.repository.OrderRepo;
import io.xmu.dataanalysis.repository.ShopDailyDataRepo;
import io.xmu.dataanalysis.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/3/4.
 */
@Service
public class HomeService {
    @Autowired
    OrderRepo orderRepo;

    @Autowired
    ShopDailyDataRepo shopDailyDataRepo;

    public Map<String, Integer> findBackLogCount() {

//        List<Order> toPay = orderRepo.findOrderByStatus(100);
//        List<Order> paid = orderRepo.findOrderByStatus(200);
//        List<Order> notComment = orderRepo.findOrderByStatus(300);
//        List<Order> commented = orderRepo.findOrderByStatus(301);
//        List<Order> complaint = orderRepo.findOrderByStatus(302);
//        List<Order> returnBack = orderRepo.findOrderByStatus(303);

        Map<String,Integer> backLog = new HashMap<>();
//        backLog.put("toPay",toPay.size());
//        backLog.put("paid",paid.size());
//        backLog.put("notComment",notComment.size());
//        backLog.put("commented",commented.size());
//        backLog.put("complaint",complaint.size());
//        backLog.put("returnBack",returnBack.size());//退款数据
        backLog.put("toPay",0);
        backLog.put("paid",0);
        backLog.put("notComment",0);
        backLog.put("commented",0);
        backLog.put("complaint",0);
        backLog.put("returnBack",0);//退款数据
        return backLog;
    }


    public Map<String,Object> getDailyData(Integer shopId){

        Map<String,Object> map = new HashMap<>();
        ShopDailyData todayData = shopDailyDataRepo.findByShopIdAndPartition(shopId, DateUtils.getToday());
        ShopDailyData yesterdayData = shopDailyDataRepo.findByShopIdAndPartition(shopId,DateUtils.getYesterday());

        map.put("refreshTime",DateUtils.getToday());

        Map<String,Number> sales = new HashMap<>();
        sales.put("today",todayData==null?0:todayData.getSales());
        sales.put("byApp",0);
        sales.put("yesterday",yesterdayData==null?0:yesterdayData.getSales());
        sales.put("rank",1);
        map.put("sales",sales);

        Map<String,Number> payedGoodsCount = new HashMap<>();
        payedGoodsCount.put("today",todayData==null?0:todayData.getPayedGoodsCount());
        payedGoodsCount.put("byApp",0);
        payedGoodsCount.put("yesterday",yesterdayData==null?0:yesterdayData.getPayedGoodsCount());
        payedGoodsCount.put("rank",1);
        map.put("payedGoodsCount",payedGoodsCount);

        Map<String,Number> payedUserCount = new HashMap<>();
        payedUserCount.put("today",todayData==null?0:todayData.getPayedUserCount());
        payedUserCount.put("byApp",0);
        payedUserCount.put("yesterday",yesterdayData==null?0:yesterdayData.getPayedUserCount());
        payedUserCount.put("rank",1);
        map.put("payedUserCount",payedUserCount);

        Map<String,Number> pv = new HashMap<>();
        pv.put("today",todayData==null?0:todayData.getPv());
        pv.put("byApp",todayData==null?0:(1.0*todayData.getPvByApp()/todayData.getPv()));
        pv.put("yesterday",yesterdayData==null?0:yesterdayData.getPv());
        pv.put("rank",1);
        map.put("pv",pv);

        Map<String,Number> uv = new HashMap<>();
        uv.put("today",todayData==null?0:todayData.getUv());
        uv.put("byApp",todayData==null?0:(1.0*todayData.getPvByApp()/todayData.getPv()));
        uv.put("yesterday",yesterdayData==null?0:yesterdayData.getUv());
        uv.put("rank",1);
        map.put("uv",uv);

        return map;
    }

}
