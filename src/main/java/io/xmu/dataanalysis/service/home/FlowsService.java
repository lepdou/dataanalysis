package io.xmu.dataanalysis.service.home;

import io.xmu.dataanalysis.entity.ShopDailyData;
import io.xmu.dataanalysis.repository.ShopDailyDataRepo;
import io.xmu.dataanalysis.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Jim Chen at XMU on 2017/4/4.
 */
@Service
public class FlowsService {

    private ShopDailyDataRepo shopDailyDataRepo;

    @Autowired
    public FlowsService(ShopDailyDataRepo shopDailyDataRepo) {
        this.shopDailyDataRepo = shopDailyDataRepo;
    }


    public Map<String, Double[]> getFlows(Integer shopId, String time) {

        HashMap<String, Double[]> flows = new HashMap<>();
        Double[] bounceRate = new Double[]{0.0, 0.0, 0.0};
        Double[] averagePV = new Double[]{0.0, 0.0, 0.0};
        Double[] averageStayTime = new Double[]{0.0, 0.0, 0.0};

        ShopDailyData today = shopDailyDataRepo.findByShopIdAndPartition(shopId, time);
        ShopDailyData yesterday = shopDailyDataRepo.findByShopIdAndPartition(shopId, DateUtils.getYesterday(time));
        ShopDailyData lastWeek = shopDailyDataRepo.findByShopIdAndPartition(shopId, DateUtils.getLastWeek(time));

        bounceRate[0] = today.getVisitOnlyOnePageUsers()*1.0/today.getUv();
        bounceRate[1] = yesterday.getVisitOnlyOnePageUsers()*1.0/today.getUv();
        bounceRate[2] = lastWeek.getVisitOnlyOnePageUsers()*1.0/today.getUv();

        bounceRate[1] = (bounceRate[0]-bounceRate[1])/bounceRate[1];
        bounceRate[2] = (bounceRate[0]-bounceRate[2])/bounceRate[2];

        //计算跳失率
        flows.put("bounceRate", bounceRate);

        int pv = today.getPv();
        int uv = today.getUv();
        averagePV[0] = pv * 1.0 / uv;
        averagePV[0] = Double.parseDouble(String.format("%.2f",averagePV[0]));
        pv = yesterday.getPv();
        uv = yesterday.getUv();
        averagePV[1] = pv * 1.0 / uv;
        averagePV[1] = (averagePV[1] - averagePV[0]) / averagePV[0];
        averagePV[1] = Double.parseDouble(String.format("%.4f",averagePV[1]));

        pv = lastWeek.getPv();
        uv = lastWeek.getUv();
        averagePV[2] = pv * 1.0 / uv;
        averagePV[2] = (averagePV[2] - averagePV[0]) / averagePV[0];
        averagePV[2] = Double.parseDouble(String.format("%.4f",averagePV[2]));


        flows.put("averagePV", averagePV);

        averageStayTime[0] = today.getAvgStayTime() * 1.0;
        averageStayTime[1] = yesterday.getAvgStayTime() * 1.0;
        averageStayTime[2] = lastWeek.getAvgStayTime() * 1.0;

        averageStayTime[1] = (averageStayTime[0] - averageStayTime[1]) / averageStayTime[1];
        averageStayTime[1] = Double.parseDouble(String.format("%.4f",averageStayTime[1]));
        averageStayTime[2] = (averageStayTime[0] - averageStayTime[2]) / averageStayTime[2];
        averageStayTime[2] = Double.parseDouble(String.format("%.4f",averageStayTime[2]));

        flows.put("averageStayTime", averageStayTime);

        return flows;
    }

    public List<Object[]> getBounceRate(Integer shopId, String time) {
        String lastWeek = DateUtils.getLastWeek(time);
        lastWeek = DateUtils.getLastWeek(lastWeek);
        List<ShopDailyData> twoWeekData = shopDailyDataRepo.findByShopIdAndPartitionBetween(shopId,lastWeek,time);
        int length = twoWeekData.size();
        LinkedList<Object[]>  data = new LinkedList<>();
        for(ShopDailyData dailyData: twoWeekData){
            Object[] d1= new Object[2];
            d1[0] = DateUtils.getWeekDay(dailyData.getPartition());
            String b = String.format("%.2f",dailyData.getVisitOnlyOnePageUsers()*1.0/dailyData.getUv());
            d1[1] = Double.parseDouble(b);
            data.addFirst(d1);
        }
        String day = twoWeekData.get(length-1).getPartition();
//        int sub = 14-length;
//        while (sub-->0){
//            day = DateUtils.getYesterday(day);
//            data.addFirst(new Object[]{DateUtils.getWeekDay(day),0.0});
//        }

        return data;
    }

    public List<Object[]> getAverageStayTime(Integer shopId, String time) {
        String lastWeek = DateUtils.getLastWeek(time);
        lastWeek = DateUtils.getLastWeek(lastWeek);
        List<ShopDailyData> twoWeekData = shopDailyDataRepo.findByShopIdAndPartitionBetween(shopId,lastWeek,time);
        int length = twoWeekData.size();
        LinkedList<Object[]>  data = new LinkedList<>();
        for(ShopDailyData dailyData: twoWeekData){
            Object[] d1= new Object[2];
            d1[0] = DateUtils.getWeekDay(dailyData.getPartition());
            d1[1] = dailyData.getAvgStayTime();
            data.addFirst(d1);
        }
        String day = twoWeekData.get(length-1).getPartition();
//        int sub = 14-length;
//        while (sub-->0){
//            day = DateUtils.getYesterday(day);
//            data.addFirst(new Object[]{DateUtils.getWeekDay(day),0.0});
//        }

        return data;
    }

    public List<Object[]> getAveragePV(Integer shopId, String time) {
        String lastWeek = DateUtils.getLastWeek(time);
        lastWeek = DateUtils.getLastWeek(lastWeek);
        List<ShopDailyData> twoWeekData = shopDailyDataRepo.findByShopIdAndPartitionBetween(shopId,lastWeek,time);

        int length = twoWeekData.size();
        LinkedList<Object[]>  data = new LinkedList<>();
        for(ShopDailyData dailyData:twoWeekData){
            Object[] d1= new Object[2];
            d1[0]= DateUtils.getWeekDay(dailyData.getPartition());
            d1[1]= Double.parseDouble(String.format("%.2f",dailyData.getPv()*1.0/dailyData.getUv()));
            data.addFirst(d1);
        }

        String day = twoWeekData.get(length-1).getPartition();
//        int sub = 14-length;
//        while(sub-->0){
//            day = DateUtils.getYesterday(day);
//            data.addFirst(new Object[]{DateUtils.getWeekDay(day),0.0});
//        }
        return data;
    }

    public static void main(String[] args){
        System.out.println(String.format("%.2f",1.233014));
    }

}
