angular.module('pVisual').component('pVisual', {
    templateUrl: 'p-visual/p-visual.html',

    controller: ['$state',function ($state) {
        var self = this;
        $state.go('visual.visual0');
    }]

});

angular.module('pVisual').component('pVisual0', {
    templateUrl: 'p-visual/p-visual-detail-0.html',
    controller: [
        function () {
            var self = this;
            var chart_left = echarts.init(document.getElementById('chart-left'));

            function randomData() {
                return Math.round(Math.random() * 1000);
            }

            var data = [{
                name: '北京',
                value: randomData()
            }, {
                name: '天津',
                value: randomData()
            }, {
                name: '上海',
                value: randomData()
            }, {
                name: '重庆',
                value: randomData()
            }, {
                name: '河北',
                value: randomData()
            }, {
                name: '河南',
                value: randomData()
            }, {
                name: '云南',
                value: randomData()
            }, {
                name: '辽宁',
                value: randomData()
            }, {
                name: '黑龙江',
                value: randomData()
            }, {
                name: '湖南',
                value: randomData()
            }, {
                name: '安徽',
                value: randomData()
            }, {
                name: '山东',
                value: randomData()
            }, {
                name: '新疆',
                value: randomData()
            }, {
                name: '江苏',
                value: randomData()
            }, {
                name: '浙江',
                value: randomData()
            }, {
                name: '江西',
                value: randomData()
            }, {
                name: '湖北',
                value: randomData()
            }, {
                name: '广西',
                value: randomData()
            }, {
                name: '甘肃',
                value: randomData()
            }, {
                name: '山西',
                value: randomData()
            }, {
                name: '内蒙古',
                value: randomData()
            }, {
                name: '陕西',
                value: randomData()
            }, {
                name: '吉林',
                value: randomData()
            }, {
                name: '福建',
                value: randomData()
            }, {
                name: '贵州',
                value: randomData()
            }, {
                name: '广东',
                value: randomData()
            }, {
                name: '青海',
                value: randomData()
            }, {
                name: '西藏',
                value: randomData()
            }, {
                name: '四川',
                value: randomData()
            }, {
                name: '宁夏',
                value: randomData()
            }, {
                name: '海南',
                value: randomData()
            }, {
                name: '台湾',
                value: randomData()
            }, {
                name: '香港',
                value: randomData()
            }, {
                name: '澳门',
                value: randomData()
            }];

            var sum = 0;
            for (var i = 0; i < data.length; i++) {
                sum += data[i].value;
            }
            ;

            data.sort(function (a, b) {
                return b.value - a.value;
            })

            var option1 = {
                title: {
                    text: '地域分布',
                    textStyle: {
                        fontWeight: 'normal',
                        fontSize: 18
                    }
                },
                tooltip: {
                    trigger: 'item',
                    formatter: function (a) {
                        return a.name + '：' + (a.value * 100 / sum).toFixed(2) + '% (' + a.value + ')';
                    }
                },
                visualMap: {
                    type: 'continuous',
                    orient: 'horizontal',
                    min: 0,
                    max: 1000,
                    left: '10%',
                    bottom: '10%',
                    calculable: true,
                    inRange: {
                        color: ['#9CD9AF', '#7FD097', '#45B765', '#229342']
                    }
                },
                series: [{
                    type: 'map',
                    map: 'china',
                    itemStyle: {
                        normal: {
                            areaColor: '#9CD9AF',
                            borderColor: '#FFFFFF'
                        },
                        emphasis: {
                            areaColor: '#5D9CEC'
                        }
                    },
                    data: data
                }]
            }

            chart_left.setOption(option1);

            var chart_right = echarts.init(document.getElementById('chart-right'));

            var topData = data.slice(0, 10);
            topData.reverse();

            var option2 = {
                title: {
                    text: 'TOP10地域分布',
                    textStyle: {
                        fontWeight: 'normal'
                    }
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{b}: {c}'
                },
                grid: {
                    left: '3%',
                    right: '10%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [{
                    type: 'value',
                    show: false
                }],
                yAxis: [{
                    type: 'category',
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    },
                    data: topData.map(function (item) {
                        return item.name;
                    })
                }],
                series: [{
                    name: '分布',
                    type: 'bar',
                    barWidth: 20,
                    barGap: '100%',
                    label: {
                        normal: {
                            show: true,
                            position: 'right',
                            textStyle: {
                                color: '#111111',
                                fontWeight: 'bolder'
                            },
                            formatter: function (a) {
                                return (a.value * 100 / sum).toFixed(2) + '%';
                            }
                        }
                    },
                    data: topData.map(function (item) {
                        return item.value;
                    })
                }],
                color: ['#45B765']

            };

            chart_right.setOption(option2);

        }
    ]
});
angular.module('pVisual').component('pVisual1', {
    templateUrl: 'p-visual/p-visual-detail-1.html',
    controller: [
        function () {
            var self = this;
//          var chart_left = echarts.init(document.getElementById('chart-gender-left'));
//          var option_left = {};
//          chart_left.setOption(option_left);

            var chart_right = echarts.init(document.getElementById('chart-gender-right'));
            var option_right = {
            	title:{
            		text: '年龄分布',
            		textStyle: {
            			fontWeight: 'normal'
            		},
            		left: 'left'
            	},
                series: [
                    {
                        name: '小青年',
                        type: 'pie',
                        radius: ['92%', '100%'],
                        startAngle: 0,
                        color: ["transparent", "#8884D8", "#EEEEEE"],
                        hoverAnimation: false,
                        silent: true,
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [180, 90, 90]
                    },
                    {
                        name: '青年',
                        type: 'pie',
                        radius: ['79%', '87%'],
                        startAngle: 0,
                        color: ["transparent", "#9CACF1", "#EEEEEE"],
                        hoverAnimation: false,
                        silent: true,
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [180, 80, 100]
                    },
                    {
                        name: '青壮年',
                        type: 'pie',
                        radius: ['66%', '74%'],
                        startAngle: 0,
                        color: ["transparent", "#9CACF1", "#EEEEEE"],
                        hoverAnimation: false,
                        silent: true,
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [180, 60, 120]
                    },
                    {
                        name: '中青年',
                        type: 'pie',
                        radius: ['53%', '61%'],
                        startAngle: 0,
                        color: ["transparent", "#82CA9D", "#EEEEEE"],
                        hoverAnimation: false,
                        silent: true,
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [180, 50, 130]
                    },
                    {
                        name: '中年',
                        type: 'pie',
                        radius: ['40%', '48%'],
                        startAngle: 0,
                        color: ["transparent", "#A4DE6C", "#EEEEEE"],
                        hoverAnimation: false,
                        silent: true,
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [180, 45, 135]
                    },
                    {
                        name: '中老年',
                        type: 'pie',
                        radius: ['27%', '35%'],
                        startAngle: 0,
                        color: ['transparent', "#D0ED57", "#EEEEEE"],
                        hoverAnimation: false,
                        silent: true,
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [180, 35, 145]
                    },
                    {
                        name: '其他',
                        type: 'pie',
                        radius: ['14%', '22%'],
                        startAngle: 0,
                        color: ['transparent', "#F7E254", '#EEEEEE'],
                        hoverAnimation: false,
                        silent: true,
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [180, 15, 165]
                    }
                ]
            };
            chart_right.setOption(option_right);

        }
    ]
});
angular.module('pVisual').component('pVisual2', {
    templateUrl: 'p-visual/p-visual-detail-2.html',
    controller: [
        function () {
            var self = this;

            var chart = echarts.init(document.getElementById('chart-2'));

            var option = {
                title: {
                    text: '商品售罄率',
                    textStyle: {
                        fontWeight: 'normal'
                    }
                },
                legend: {
                    data: ['卫衣', '打底裤', '毛衣', '牛仔裤', '皮衣', '羽绒服', '衬衫', '连衣裙', '风衣'],
                    orient: 'vertical',
                    align: 'left',
                    left: 'right',

                },
                yAxis: {
                    type: 'value',
                    name: '售罄率',
                    min: 0.0,
                    max: 0.8,
                    interval: 0.2,
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    markLine: []
                },
                xAxis: {
                    data: ['2017-02-01', '2017-03-01', '2017-04-01'],
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    }
                },
                series: [{
                    name: '卫衣',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        }
                    },
                    data: [0.49, 0.59, 0.20]
                }, {
                    name: '打底裤',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        }
                    },
                    data: [0.56, 0.67, 0.67]
                }, {
                    name: '毛衣',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        }
                    },
                    data: [0.68, 0.67, 0.63]
                }, {
                    name: '牛仔裤',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        }
                    },
                    data: [0.75, 0.63, 0.63]
                }, {
                    name: '皮衣',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        }
                    },
                    data: [0.46, 0.52, 0.35]
                }, {
                    name: '羽绒服',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        }
                    },
                    data: [0.40, 0.63, 0.58]
                }, {
                    name: '衬衫',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        }
                    },
                    data: [0.19, 0.31, 0.28]
                }, {
                    name: '连衣裙',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        }
                    },
                    data: [0.28, 0.38, 0.23]
                }, {
                    name: '风衣',
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#111'
                            }
                        },
                    },
                    data: [0.46, 0.50, 0.53],
                    markLine: {
                        silent: true,
                        data: [{
                            yAxis: 0.6
                        }],
                        lineStyle: {
                            normal: {
                                color: '#222'
                            }
                        }
                    }
                }],
                color: ['#2DB3E4', '#FF8500', '#8C98CC', '#BA8B37', '#B6D0DE', '#FFD0B6', '#006BC2', '#197C90', '#72C8F2']
            }

            chart.setOption(option);

        }
    ]

});
angular.module('pVisual').component('pVisual3', {
    templateUrl: 'p-visual/p-visual-detail-3.html',
    controller: [
        function () {
            var self = this;
            var chart = echarts.init(document.getElementById('chart-store'));
            var option = {
                title: {
                    text: '商品库销比',
                    textStyle: {
                        fontWeight: 'normal'
                    }
                },
                legend: {
                    right: '1%',
                    data: ['库销比_8月', '库销比_9月', '销售件数_8月', '销售件数_9月']
                },
                xAxis: {
                    type: 'category',
                    data: ['卫衣', '打底裤', '毛衣', '牛仔裤', '皮衣', '羽绒服', '衬衫', '连衣裙', '风衣'],
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    }
                },
                yAxis: [{
                    type: 'value',
                    name: '库销比',
                    min: 0,
                    max: 4,
                    interval: 1,
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    }
                }, {
                    type: 'value',
                    name: '销售件数',
                    min: 0,
                    max: 200,
                    interval: 50,
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    }
                }],
                series: [{
                    name: '库销比_8月',
                    type: 'bar',
                    barWidth: '25%',
                    label: {
                        normal: {
                            show: true,
                            formatter: '{c}',
                            position: 'top',
                            textStyle: {
                                color: '#111111',
                                fontWeight: 'bolder'
                            }
                        }
                    },
                    data: [2.17, 2.25, 3.64, 3.65, 3.49, 1.76, 1.25, 1.52, 2.20]
                }, {
                    name: '库销比_9月',
                    type: 'bar',
                    barWidth: '25%',
                    label: {
                        normal: {
                            show: true,
                            formatter: '{c}',
                            position: 'top',
                            textStyle: {
                                color: '#111111',
                                fontWeight: 'bolder'
                            }
                        }
                    },
                    data: [3.57, 3.00, 3.64, 3.31, 3.57, 3.03, 1.49, 1.85, 2.17]
                }, {
                    name: '销售件数_8月',
                    type: 'line',
                    smooth: true,
                    yAxisIndex: 1,
                    label: {
                        normal: {
                            show: true,
                            formatter: '{c}',
                            position: 'left',
                            textStyle: {
                                color: '#111111',
                                fontWeight: 'bolder'
                            }
                        }
                    },
                    data: [53, 120, 68, 83, 50, 62, 25, 32, 139]
                }, {
                    name: '销售件数_9月',
                    type: 'line',
                    smooth: true,
                    yAxisIndex: 1,
                    label: {
                        normal: {
                            show: true,
                            formatter: '{c}',
                            position: 'right',
                            textStyle: {
                                color: '#111111',
                                fontWeight: 'bolder'
                            }
                        }
                    },
                    data: [54, 40, 68, 66, 40, 62, 28, 32, 159]
                }],
                color: ['#4D5FB0', '#90C8F2', '#4D5FB0', '#90C8F2']
            };
            chart.setOption(option);
        }
    ]
});
angular.module('pVisual').component('pVisual4', {
    templateUrl: 'p-visual/p-visual-detail-4.html',
    controller: [
        function () {
            var self = this;
            var chart = echarts.init(document.getElementById('chart-radar'));

            var option = {
                title: {
                    text: '支付方式分布',
                    textStyle: {
                        fontWeight: 'normal'
                    }
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['支付方式']
                },
                radar: {
                    name: {
                        formatter: '{value}',
                        textStyle: {
                            color: '#666666'
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999999'
                        }
                    },
                    //					tooltip: {
                    //						formatter: '{a}'
                    //					},
                    indicator: [{
                        name: '银行卡',
                        max: 6500
                    }, {
                        name: 'QQ钱包',
                        max: 6500
                    }, {
                        name: '信用卡',
                        max: 6500
                    }, {
                        name: '充值卡',
                        max: 6500
                    }, {
                        name: '分期付款',
                        max: 6500
                    }, {
                        name: '微信',
                        max: 6500
                    }, {
                        name: '找人代付',
                        max: 6500
                    }, {
                        name: '支付宝',
                        max: 6500
                    }, {
                        name: '网上银行',
                        max: 6500
                    }, {
                        name: '货到付款',
                        max: 6500
                    }, {
                        name: '银联在线',
                        max: 6500
                    }]
                },
                series: [{
                    name: '支付方式',
                    type: 'radar',
                    itemStyle: {
                        normal: {
                            color: '#5182E4'
                        }
                    },
                    label: {
                        normal: {
                            show: true,
                            formatter: '{c}'
                        }
                    },
                    data: [{
                        value: [3000, 4200, 3600, 2500, 5000, 4600, 5000, 4200, 3900, 5600, 3800],
                        name: '支付方式'
                    }]
                }]

            }
            chart.setOption(option);

        }
    ]
});