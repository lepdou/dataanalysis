angular.module('pHome').component('market',{
	
	templateUrl: 'p-home/market/market.html',
	
	controller: ['marketService',function marketCtrl(marketService){
		
		var self = this;

        var yesterday = function(){
            var d1= new Date().getTime()- 24 * 60 * 60 * 1000;
            d1 = new Date(d1);
            var mm = d1.getMonth() + 1;
            if(mm < 10) mm = '0' + mm;

            var dd = d1.getDate();
            if(dd < 10) dd = '0' + dd;
            return d1.getFullYear() + "-" +mm+"-"+dd;
        };

        var today = function(){
            var d1= new Date();
            var mm = d1.getMonth() + 1;
            if(mm < 10) mm = '0' + mm;

            var dd = d1.getDate();
            if(dd < 10) dd = '0' + dd;
            return d1.getFullYear() + "-" +mm+"-"+dd;
        };
		
		marketService.getSearchKeywordTop10(today()).then(function(data){
			self.keywordTop10 = data;
		});
		
		marketService.getGoodTradeTop10(today()).then(function(data){
			self.goodTradeTop10 = data;
		});
		
		marketService.getShopFlowTop10(today()).then(function(data){
			self.shopFlowTop10 = data;
		});
		
	}]
});