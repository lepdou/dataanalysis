angular.module('pHome').component('flow', {

	templateUrl: 'p-home/flow/flow.html',

	controller: ['flowService', function flowCtrl(flowService) {
		var self = this;
		
		var shopId ='100';

		flowService.getFlows(shopId).then(function(data) {
			self.flow = data;
		});

		//需要导入数据
		var myChart1 = echarts.init(document.getElementById('flow1'));

		flowService.getBounceRate(shopId).then(function(data) {
			var option = {
				tooltip: {
					trigger: 'axis'
				},
				legend: {
                    right: '1%',
                    top: '10%',
					data: ['本周', '上周']
				},
				grid: {
					left: '1%',
					right: '4%',
					bottom: '3%',
					containLabel: true
				},
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: data.slice(0, 7).map(function(item) {
						return item[0];
					})
				}],
				yAxis: [{
					type: 'value'
				}],
				series: [{
					name: '本周',
					type: 'line',
					data: data.slice(7, 14).map(function(item) {
						return item[1];
					})
				}, {
					name: '上周',
					type: 'line',
					data: data.slice(0, 7).map(function(item) {
						return item[1];
					})
				}]
			};

			myChart1.setOption(option);

		});

		var myChart2 = echarts.init(document.getElementById('flow2'));

		flowService.getAveragePV(shopId).then(function(data) {
			var option = {
				tooltip: {
					trigger: 'axis'
				},
				legend: {
                    right: '1%',
                    top: '10%',
					data: ['本周', '上周']
				},
				grid: {
					left: '1%',
					right: '4%',
					bottom: '3%',
					containLabel: true
				},
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: data.slice(0, 7).map(function(item) {
						return item[0];
					})
				}],
				yAxis: [{
					type: 'value'
				}],
				series: [{
					name: '本周',
					type: 'line',
					data: data.slice(7, 14).map(function(item) {
						return item[1];
					})
				}, {
					name: '上周',
					type: 'line',
					data: data.slice(0, 7).map(function(item) {
						return item[1];
					})
				}]
			};

			myChart2.setOption(option);

		});

		var myChart3 = echarts.init(document.getElementById('flow3'));

		flowService.getAverageStayTime(shopId).then(function(data) {
			var option = {
				tooltip: {
					trigger: 'axis'
				},
				legend: {
                    right: '1%',
                    top: '10%',
					data: ['本周', '上周']
				},
				grid: {
					left: '1%',
					right: '4%',
					bottom: '3%',
					containLabel: true
				},
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: data.slice(0, 7).map(function(item) {
						return item[0];
					})
				}],
				yAxis: [{
					type: 'value'
				}],
				series: [{
					name: '本周',
					type: 'line',
					data: data.slice(7, 14).map(function(item) {
						return item[1];
					})
				}, {
					name: '上周',
					type: 'line',
					data: data.slice(0, 7).map(function(item) {
						return item[1];
					})
				}]
			};

			myChart3.setOption(option);

		});
	}]
})