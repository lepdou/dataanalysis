angular.module('pHome').component('trade', {

	templateUrl: 'p-home/trade/trade.html',

	controller: ['tradeService', function tradeCtrl(tradeService) {

		var self = this;
		var shopId = '100';
		
		tradeService.getTrade(shopId).then(function(data) {
			self.trade = data;
		});

		var tradeChart1 = echarts.init(document.getElementById('trade1'));
		tradeService.getOrderUserCount(shopId).then(function(data) {
			var option = {
				tooltip: {
					trigger: 'axis'
				},
				legend: {
                    right: '1%',
                    top: '10%',
					data: ['本周', '上周']
				},
				grid: {
					left: '1%',
					right: '4%',
					bottom: '3%',
					containLabel: true
				},
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: data.slice(0, 7).map(function(item) {
						return item[0];
					})
				}],
				yAxis: [{
					type: 'value'
				}],
				series: [{
					name: '本周',
					type: 'line',
					data: data.slice(7, 14).map(function(item) {
						return item[1];
					})
				}, {
					name: '上周',
					type: 'line',
					data: data.slice(0, 7).map(function(item) {
						return item[1];
					})
				}]
			};
			tradeChart1.setOption(option);
		});

		var tradeChart2= echarts.init(document.getElementById('trade2'));
		tradeService.getPayedUserCount(shopId).then(function(data) {
			var option = {
				tooltip: {
					trigger: 'axis'
				},
				legend: {
                    right: '1%',
                    top: '10%',
					data: ['本周', '上周']
				},
				grid: {
					left: '1%',
					right: '4%',
					bottom: '3%',
					containLabel: true
				},
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: data.slice(0, 7).map(function(item) {
						return item[0];
					})
				}],
				yAxis: [{
					type: 'value'
				}],
				series: [{
					name: '本周',
					type: 'line',
					data: data.slice(7, 14).map(function(item) {
						return item[1];
					})
				}, {
					name: '上周',
					type: 'line',
					data: data.slice(0, 7).map(function(item) {
						return item[1];
					})
				}]
			};
			tradeChart2.setOption(option);
		});

		var tradeChart3 = echarts.init(document.getElementById('trade3'));
		tradeService.getPayedGoodCount(shopId).then(function(data) {
			var option = {
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					right: '1%',
					top: '10%',
					data: ['本周', '上周']
				},
				grid: {
					left: '1%',
					right: '4%',
					bottom: '3%',
					containLabel: true
				},
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: data.slice(0, 7).map(function(item) {
						return item[0];
					})
				}],
				yAxis: [{
					type: 'value'
				}],
				series: [{
					name: '本周',
					type: 'line',
					data: data.slice(7, 14).map(function(item) {
						return item[1];
					})
				}, {
					name: '上周',
					type: 'line',
					color: '#EEEEEE',
					data: data.slice(0, 7).map(function(item) {
						return item[1];
					})
				}]
			};
			tradeChart3.setOption(option);
			
		});

	}]
});