angular.module('pHome').component('customer', {
	templateUrl: 'p-home/customer/customer.html',

	controller: ['customerService', function(customerService) {
		var self = this;

		var shopId = 100;

		var customer = echarts.init(document.getElementById('pie'));

		customerService.getCustomerClassification(shopId).then(function(data) {
			option = {
				tooltip: {
					trigger: 'item',
					formatter: "{a} <br/>{b}: {c} ({d}%)"
				},
				legend: {
					orient: 'vertical',
					x: 'left',
					data: data.map(function(item) {
						return item.name;
					})
				},
				series: [{
					name: '访问来源',
					type: 'pie',
					radius: ['45%', '80%'],
					avoidLabelOverlap: false,
					label: {
						normal: {
							show: false,
							position: 'center'
						},
						emphasis: {
							show: true,
							textStyle: {
								fontSize: '30',
								fontWeight: 'bold'
							}
						}
					},
					labelLine: {
						normal: {
							show: false
						}
					},
					data: data
				}],
				color: ['#81A8EB','#64B66C','#E9AC51','#AED4C2','#DDA490']
			};

			customer.setOption(option);

		});

	}]

});