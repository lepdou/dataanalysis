angular.module('pHome').component('pHome', {

	templateUrl: 'p-home/p-home.html',

	controller: ['homeService', 'marketService', 'goodService', function pHomeCtrl(homeService, marketService, goodService) {

		var self = this;

		var shopId = '100';

		//backlog.....start.............
		homeService.getBacklog(shopId).then(function(data) {
			self.backlog = data;
		});
		//backlog.....end..............

		//indices.....start............

		self.selectedTable = 'table0';

		homeService.getDataSummery(shopId).then(function(data) {
			self.refreshTime = data[0];
			self.dateTime = data[2];
			self.maxDate = data[2];
			self.minDate = nextMonth(data[1]);
			self.category = data[3];
			self.pic = data[4];
			self.name = data[5];
		});

		homeService.getIndices(shopId, self.dateTime).then(function(data) {
			self.indices = data;
		});

		var indexChart0 = echarts.init(document.getElementById('indexChart0'));
		var indexChart1 = echarts.init(document.getElementById('indexChart1'));
		var indexChart2 = echarts.init(document.getElementById('indexChart2'));
		var indexChart3 = echarts.init(document.getElementById('indexChart3'));
		var indexChart4 = echarts.init(document.getElementById('indexChart4'));

		homeService.getSales(shopId).then(function(data) {

			var data1 = data.map(function(item) {
				return item[1];
			});

			var option = {
				title: {
					text: '近30天平均：' + average(data1.slice(data.length - 31, data.length - 1))
				},
				toolbox: {
					feature: {
						dataView: {
							show: true,
							readOnly: false
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				legend: {
					data: ['我的支付金额', '同行平均支付金额', '同行优秀支付金额']
				},
				xAxis: {
					data: data.map(function(item) {
						return item[0];
					})
				},
				yAxis: {},
				dataZoom: [{
					disabled: true,
					startValue: data.length - 31,
					endValue: data.length - 1,
					zoomLock: true
				}],
				series: [{
					name: '我的支付金额',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[1];
					})
				}, {
					name: '同行平均支付金额',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[2];
					})
				}, {
					name: '同行优秀支付金额',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[3];
					})
				}]
			};
			indexChart0.setOption(option);

		});

		homeService.getPayedGoodsCount(shopId).then(function(data) {
			var data1 = data.map(function(item) {
				return item[1];
			});
			var option = {
				title: {
					text: '近30天平均：' + average(data1.slice(data.length - 31, data.length - 1))
				},
				toolbox: {
					feature: {
						dataView: {
							show: true,
							readOnly: false
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				legend: {
					data: ['我的支付商品数', '同行平均支付商品数', '同行优秀支付商品数']
				},
				xAxis: {
					data: data.map(function(item) {
						return item[0];
					})
				},
				yAxis: {},
				dataZoom: [{
					disabled: true,
					startValue: data.length - 31,
					endValue: data.length - 1,
					zoomLock: true
				}],
				series: [{
					name: '我的支付商品数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[1];
					})
				}, {
					name: '同行平均支付商品数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[2];
					})
				}, {
					name: '同行优秀支付商品数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[3];
					})
				}]
			};
			indexChart1.setOption(option);

		});

		homeService.getPayedUserCount(shopId).then(function(data) {
			var data1 = data.map(function(item) {
				return item[1];
			});
			var option = {
				title: {
					text: '近30天平均：' + average(data1.slice(data.length - 31, data.length - 1))
				},
				toolbox: {
					feature: {
						dataView: {
							show: true,
							readOnly: false
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				legend: {
					data: ['我的支付买家数', '同行平均支付买家数', '同行优秀支付买家数']
				},
				xAxis: {
					data: data.map(function(item) {
						return item[0];
					})
				},
				yAxis: {},
				dataZoom: [{
					disabled: true,
					startValue: data.length - 31,
					endValue: data.length - 1,
					zoomLock: true
				}],
				series: [{
					name: '我的支付买家数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[1];
					})
				}, {
					name: '同行平均支付买家数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[2];
					})
				}, {
					name: '同行优秀支付买家数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[3];
					})
				}]
			};
			indexChart2.setOption(option);

		});

		homeService.getPV(shopId).then(function(data) {
			var data1 = data.map(function(item) {
				return item[1];
			});
			var option = {
				title: {
					text: '近30天平均：' + average(data1.slice(data.length - 31, data.length - 1))
				},
				toolbox: {
					feature: {
						dataView: {
							show: true,
							readOnly: false
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				legend: {
					data: ['我的支付浏览量', '同行平均支付浏览量', '同行优秀支付浏览量']
				},
				xAxis: {
					data: data.map(function(item) {
						return item[0];
					})
				},
				yAxis: {},
				dataZoom: [{
					disabled: true,
					startValue: data.length - 31,
					endValue: data.length - 1,
					zoomLock: true
				}],
				series: [{
					name: '我的支付浏览量',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[1];
					})
				}, {
					name: '同行平均支付浏览量',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[2];
					})
				}, {
					name: '同行优秀支付浏览量',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[3];
					})
				}]
			};
			indexChart3.setOption(option);
		});

		homeService.getUV(shopId).then(function(data) {
			var data1 = data.map(function(item) {
				return item[1];
			});
			var option = {
				title: {
					text: '近30天平均：' + average(data1.slice(data.length - 31, data.length - 1))
				},
				toolbox: {
					feature: {
						dataView: {
							show: true,
							readOnly: false
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				legend: {
					data: ['我的支付访客数', '同行平均支付访客数', '同行优秀支付访客数']
				},
				xAxis: {
					data: data.map(function(item) {
						return item[0];
					})
				},
				yAxis: {},
				dataZoom: [{
					disabled: true,
					startValue: data.length - 31,
					endValue: data.length - 1,
					zoomLock: true
				}],
				series: [{
					name: '我的支付访客数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[1];
					})
				}, {
					name: '同行平均支付访客数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[2];
					})
				}, {
					name: '同行优秀支付访客数',
					type: 'line',
					smooth: true,
					data: data.map(function(item) {
						return item[3];
					})
				}]
			};
			indexChart4.setOption(option);
		});

		var average = function(data) {
			var sum = 0;
			data.forEach(function(e) {
				sum += e;
			});
			return(sum / data.length).toFixed(2);
		};

		self.nextDay = function() {
			var nDay = new Date(self.dateTime).getTime() + 24 * 60 * 60 * 1000;
			var nD = new Date(nDay);

			var mm = nD.getMonth() + 1;
			if(mm < 10) mm = '0' + mm;

			var dd = nD.getDate();
			if(dd < 10) dd = '0' + dd;

			self.dateTime = nD.getFullYear() + '-' + mm + '-' + dd;

			self.onChange();
		};

		self.lastDay = function() {
			var nDay = new Date(self.dateTime).getTime() - 24 * 60 * 60 * 1000;
			var nD = new Date(nDay);

			var mm = nD.getMonth() + 1;
			if(mm < 10) mm = '0' + mm;

			var dd = nD.getDate();
			if(dd < 10) dd = '0' + dd;

			self.dateTime = nD.getFullYear() + '-' + mm + '-' + dd;

			self.onChange();
		};

		var lastMonth = function(today) {
			var nDay = new Date(today).getTime() - 24 * 60 * 60 * 1000 * 30;
			var nD = new Date(nDay);

			var mm = nD.getMonth() + 1;
			if(mm < 10) mm = '0' + mm;

			var dd = nD.getDate();
			if(dd < 10) dd = '0' + dd;

			return nD.getFullYear() + '-' + mm + '-' + dd;
		};

		var nextMonth = function(today) {
			var nDay = new Date(today).getTime() + 24 * 60 * 60 * 1000 * 30;
			var nD = new Date(nDay);

			var mm = nD.getMonth() + 1;
			if(mm < 10) mm = '0' + mm;

			var dd = nD.getDate();
			if(dd < 10) dd = '0' + dd;

			return nD.getFullYear() + '-' + mm + '-' + dd;
		};

		self.onChange = function() {

			var dd = new Date(self.dateTime).getTime();
			var md = new Date(self.maxDate).getTime();
			var nd = new Date(self.minDate).getTime();

			if(dd < nd) {
				self.dateTime = self.minDate;
				self.btnLeftDisable = true;
				return;
			}
			if(dd > md) {
				self.dateTime = self.maxDate;
				self.btnRightDisable = true;
				return;
			}
			self.btnLeftDisable = false;
			self.btnRightDisable = false;

			//更改数据

			indexChart0.dispatchAction({
				type: 'dataZoom',
				startValue: lastMonth(self.dateTime),
				endValue: self.dateTime
			});
			indexChart1.dispatchAction({
				type: 'dataZoom',
				startValue: lastMonth(self.dateTime),
				endValue: self.dateTime
			});
			indexChart2.dispatchAction({
				type: 'dataZoom',
				startValue: lastMonth(self.dateTime),
				endValue: self.dateTime
			});
			indexChart3.dispatchAction({
				type: 'dataZoom',
				startValue: lastMonth(self.dateTime),
				endValue: self.dateTime
			});
			indexChart4.dispatchAction({
				type: 'dataZoom',
				startValue: lastMonth(self.dateTime),
				endValue: self.dateTime
			});

			var option0 = indexChart0.getOption();
			var start = option0.dataZoom[0].startValue + 1;
			var end = option0.dataZoom[0].endValue + 1;
			var data = option0.series[0].data.slice(start, end);
			indexChart0.setOption({
				title: {
					text: '近30天平均：' + average(data)
				}
			});

			var option1 = indexChart1.getOption();
			start = option1.dataZoom[0].startValue + 1;
			end = option1.dataZoom[0].endValue + 1;
			data = option1.series[0].data.slice(start, end);
			indexChart1.setOption({
				title: {
					text: '近30天平均：' + average(data)
				}
			});

			var option2 = indexChart2.getOption();
			start = option2.dataZoom[0].startValue + 1;
			end = option2.dataZoom[0].endValue + 1;
			data = option2.series[0].data.slice(start, end);
			indexChart2.setOption({
				title: {
					text: '近30天平均：' + average(data)
				}
			});

			var option3 = indexChart3.getOption();
			start = option3.dataZoom[0].startValue + 1;
			end = option3.dataZoom[0].endValue + 1;
			data = option3.series[0].data.slice(start, end);
			indexChart3.setOption({
				title: {
					text: '近30天平均：' + average(data)
				}
			});

			var option4 = indexChart4.getOption();
			start = option4.dataZoom[0].startValue + 1;
			end = option4.dataZoom[0].endValue + 1;
			data = option4.series[0].data.slice(start, end);
			indexChart4.setOption({
				title: {
					text: '近30天平均：' + average(data)
				}
			});

			homeService.getIndices(shopId, self.dateTime).then(function(data) {
				self.indices = data;
			});

		};

		//indices.....end..............

		//good........start..............

		var funnelChart = echarts.init(document.getElementById('funnel'));

		goodService.getGoodData(shopId).then(function(data) {
			self.good = data;

			self.orderGoodWidth = {
				width: self.good.orderGood[0] / self.good.goodView[0] * 100 + '%'
			};
			self.payedGoodWidth = {
				width: self.good.payedGood[0] / self.good.goodView[0] * 100 + '%'
			};

			var option = {
				calculable: false,
				series: [{
					type: 'funnel',
					left: 0,
					top: 0,
					width: '100%',
					height: '100%',
					minSize: '20%',
					maxSize: '60%',
					sort: 'descending',
					gap: 2,
					labelLine: {
						normal: {
							length: 10,
							lineStyle: {
								width: 1,
								type: 'solid'
							}
						}
					},
					data: [{
						value: 60,
						name: '访问' + '\n' + (self.good.orderGood[0] / self.good.goodView[0] * 100).toFixed(1) + '%',
						itemStyle: {
							normal: {
								color: '#81A8EB'
							}
						}
					}, {
						value: 40,
						name: '下单' + '\n' + (self.good.payedGood[0] / self.good.orderGood[0] * 100).toFixed(1) + '%',
						itemStyle: {
							normal: {
								color: '#64B66C'
							}
						}
					}, {
						value: 20,
						name: '支付' + '\n' + (self.good.payedGood[0] / self.good.goodView[0] * 100).toFixed(1) + '%',
						itemStyle: {
							normal: {
								color: '#E9AC51'
							}
						}
					}]
				}]
			};

			funnelChart.setOption(option);
		});

		//good........end................

		//home-right.......start.......................................

		var yesterday = function() {
			var d1 = new Date().getTime() - 24 * 60 * 60 * 1000;
			d1 = new Date(d1);
			var mm = d1.getMonth() + 1;
			if(mm < 10) mm = '0' + mm;

			var dd = d1.getDate();
			if(dd < 10) dd = '0' + dd;
			return d1.getFullYear() + "-" + mm + "-" + dd;
		};

		var today = function() {
			var d1 = new Date();
			var mm = d1.getMonth() + 1;
			if(mm < 10) mm = '0' + mm;

			var dd = d1.getDate();
			if(dd < 10) dd = '0' + dd;
			return d1.getFullYear() + "-" + mm + "-" + dd;
		};

		marketService.getSalesTop10(today()).then(function(data) {
			self.salesTop10 = data;
		});

		self.selectedList = 'today';

		marketService.getGoodViewTop10(today()).then(function(data) {
			self.goodViewTop10_1 = data;
		});

		marketService.getGoodViewTop10(yesterday()).then(function(data) {
			self.goodViewTop10_2 = data;
		})

		//home-right.......end.........................................

	}]
});