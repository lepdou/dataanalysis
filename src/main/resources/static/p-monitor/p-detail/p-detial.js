angular.module('pMonitor').component('pDetail', {
	templateUrl: 'p-monitor/p-detail/p-detail.html',
	controller: ['detailService', function(detailService) {
		var self = this;

		self.selectedTab = 'tab0';

		self.monitorShops = [
			'柚子美衣',
			'品胜数码旗舰店',
			'romoss旗舰店',
			'品胜配点专卖店',
			'小米官方旗舰店',
			'yoobao旗舰店',
			'菁创数码专营店',
			'品胜北京专卖店',
			'闪魔旗舰店'
		];
		self.selectShop = '柚子美衣';

		var chart1 = echarts.init(document.getElementById('chart1'));
		var chart2 = echarts.init(document.getElementById('chart2'));
		var chart3 = echarts.init(document.getElementById('chart3'));
		var chart4 = echarts.init(document.getElementById('chart4'));

		option = {
			title: {
				text: '销量',
				left: '2%'
			},
			tooltip: {
				trigger: 'axis'
			},
			legend: {
				left: '2%',
				bottom: '0%',
				data: ['销量']
			},
			grid: {
				left: '2%',
				right: '4%',
				top: '25%',
				bottom: '10%',
				containLabel: true
			},
			toolbox: {
				feature: {
					dataView: {
						show: true,
						readOnly: false
					},
					magicType: {
						show: true,
						type: ['line', 'bar']
					},
					restore: {
						show: true
					},
					saveAsImage: {
						show: true
					}
				}
			},
			xAxis: {
				type: 'category',
				boundaryGap: false,
				data: ['4-23', '4-24', '4-25', '4-26', '4-27', '4-28', '4-29']
			},
			yAxis: {
				type: 'value'
			},
			series: [{
				name: '销量',
				type: 'line',
				smooth: true,
				stack: '总量',
				cololr: '#E67267',
				areaStyle: {
					normal: {
						opacity: 0.1
					}
				},
				data: [1078, 1733, 1919, 1298, 1262, 2469, 844]
			}]
		};

		chart1.setOption(option);
		chart2.setOption(option);
		chart3.setOption(option);
		chart4.setOption(option);

		self.arrays = [1, 2, 3];

		var record = echarts.init(document.getElementById('record'));

		data = [
			['3-1',
				50,
				20,
			],
			['3-2',
				80,
				40,
			],
			['3-3',
				100,
				50,
			],
			['3-4',
				150,
				80,
			],
			['3-5',
				200,
				100,
			],

			['3-6',
				260,
				120,
			],
			['3-7',
				350,
				150,
			],
			['3-8',
				380,
				170,
			],
			['3-9',
				400,
				200,
			],
			['3-10',
				450,
				250,
			],
			['3-11',
				500,
				290,
			],

			['3-12',
				550,
				300,
			],
			['3-13',
				600,
				350,
			],
			['3-14',
				690,
				380,
			],
			['3-15',
				700,
				400,
			],

			['3-16',
				800,
				360,
			],
			['3-17',
				1000,
				300,
			],
			['3-18',
				1200,
				260,
			],
			['3-19',
				1100,
				240,
			],
			['3-20',
				1000,
				220,
			],
			['3-21',
				900,
				200,
			],
			['3-22',
				850,
				170,
			],
			['3-23',
				640,
				150,
			],
			['3-24',
				600,
				130,
			],
			['3-25',
				500,
				100,
			],

			['3-26',
				400,
				90,
			],
			['3-27',
				380,
				60,
			],
			['3-28',
				350,
				50,
			],
			['3-29',
				300,
				40,
			],
			['3-30',
				230,
				30,
			],
			['3-31',
				100,
				10,
			]
		]

		var recordOption = {
			title: {
				text: '最近近30天成交趋势',
				textStyle: {
					fontWeight: 'normal',
					fontSize: 15
				}
			},
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'cross',
					crossStyle: {
						color: '#999'
					}
				}
			},
			toolbox: {
				right: '2%',
				feature: {
					dataView: {
						show: true,
						readOnly: false
					},
					magicType: {
						show: true,
						type: ['line', 'bar']
					},
					restore: {
						show: true
					},
					saveAsImage: {
						show: true
					}
				}
			},
			legend: {
				data: ['估计销售额（元）', '售件数（件）']
			},
			grid: {
				top: '20%',
				left: '5%'
			},
			xAxis: [{
				type: 'category',
				data: data.map(function(item){
					return item[0];
				}),
				axisPointer: {
					type: 'shadow'
				}
			}],
			yAxis: [{
				type: 'value',
				name: '估计销售额（元）',
				min: 0,
				//				max: 120000,
//				interval: 500,
				axisLabel: {
					formatter: '{value} 元'
				}
			}, {
				type: 'value',
				name: '销售件数（件）',
				min: 0,
				//				max: 3500,
//				interval: 50,
				axisLabel: {
					formatter: '{value} 件'
				}
			}],
			series: [{
				name: '估计销售额（元）',
				type: 'bar',
				data:  data.map(function(item){
					return item[1];
				})
			}, {
				name: '售件数（件）',
				type: 'line',
				smooth: true,
				yAxisIndex: 1,
				data:  data.map(function(item){
					return item[2];
				})
			}]
		};

		record.setOption(recordOption);
		self.recordShow = false;

		showRecord = function() {
			self.recordShow = true;
		}

		hideRecord = function() {
			self.recordShow = false;
			console.log('hello');
		}

	}]
});