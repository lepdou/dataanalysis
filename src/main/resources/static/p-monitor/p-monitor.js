angular.module('pMonitor').component('pMonitor', {
    templateUrl: 'p-monitor/p-monitor.html',
    controller: ['$window', 'EventCenter', 'monitorService',
                 function pMonitorCtrl($window, EventCenter, monitorService) {
                     var self = this;

                     //todo 替换成当前登录的shopId
                     var mainShopId = 100;

                     self.addMonitor = function () {
                         $("#addMonitorDialog").modal();
                     };

                     monitorService.getMonitorShops(mainShopId).then(function (data) {
                         self.monitorShops = data;
                         console.log(data);
                     });


                     EventCenter.subscribe(EventCenter.EventType.ADD_MONITOR_SHOP,
                         function (context) {
                         monitorService.addMonitorShop(mainShopId, context.shopName).then(function (result) {
                             if (result) {
                                 alert("添加成功");
                                 $("#addMonitorDialog").modal("hide");
                                 $window.location.reload(true);
                             } else {
                                 alert("添加失败, 商户不存在");
                             }
                         });
                     })

                 }]
});
