angular.module('pocket').config(function ($stateProvider, $locationProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');
    $locationProvider.hashPrefix('');

    var states = [
        {
            name: 'home',
            url: '/home',
            component: 'pHome'
        },
        {
            name: 'monitor',
            url: '/monitor',
            component: 'pMonitor'
        }, {
            name: 'detail',
            url: '/monitor/detail',
            component: 'pDetail'
        },{
        	name: 'visual',
        	url: '/visual',
        	component: 'pVisual'
        },{
        	name: 'visual.visual0',
        	url: '/visual/visual-detail-0',
        	component: 'pVisual0'
        },{
        	name: 'visual.visual1',
        	url: '/visual/visual-detail-1',
        	component: 'pVisual1'
//      	default: true
        },{
        	name: 'visual.visual2',
        	url: '/visual/visual-detail-2',
        	component: 'pVisual2'
        },{
        	name: 'visual.visual3',
        	url: '/visual/visual-detail-3',
        	component: 'pVisual3'
        },{
        	name: 'visual.visual4',
        	url: '/visual/visual-detail-4',
        	component: 'pVisual4'
        }

    ];

    states.forEach(function (state) {
        $stateProvider.state(state);
    });

});
