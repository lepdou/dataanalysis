pocket.controller('IndexController',
                  ['$scope', 'EventCenter', IndexController]);

function IndexController($scope, EventCenter) {
    
    $scope.addMonitorShop = function () {
        EventCenter.emit(EventCenter.EventType.ADD_MONITOR_SHOP,
                         {
                             shopName: $scope.monitorShopName
                         });
    };

}
