angular.module('pHome').service('testService', function($http) {
	var service = {
		getUser: function() {
			return $http({
				method: 'GET',
				url: '/names',
				params: {
					"name": "zhangsan",
					"age": "18"
				}
			}).then(function successCallback(response) {
				return response.data;
			}, function errorCallback(response) {
				return response.data;
			});
		},
		getData: function() {
			return $http({
				method: 'GET',
				url: 'data/beijing.json'
			}).then(function successCallback(response) {
				return response.data;
			}, function errorCallback(response) {
				return response.data;
			});
		}
	}
	return service;
});

angular.module('pHome').service('homeService', function($http) {
	var service = {
		//
		getBacklog: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/backlog',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		//
		getIndices: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/indices',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		//获得销售的数据
		getSales: function(shopId) {
			return $http({
				method: 'GET',
				url: '/api/home/sales',
				params: {
					shop_id: shopId
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},

		//
		getPayedGoodsCount: function(shopId) {
			return $http({
				method: 'GET',
				url: '/api/home/payed-goods-count',
				params: {
					shop_id: shopId
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},

		getPayedUserCount: function(shopId) {
			return $http({
				method: 'GET',
				url: '/api/home/payed-user-count',
				params: {
					shop_id: shopId
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},

		getUV: function(shopId) {
			return $http({
				method: 'GET',
				url: '/api/home/uv',
				params: {
					shop_id: shopId
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getPV: function(shopId) {
			return $http({
				method: 'GET',
				url: '/api/home/pv',
				params: {
					shop_id: shopId
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getDataSummery: function(shopId) {
			return $http({
				method: 'GET',
				url: '/api/home/summery',
				params: {
					shop_id: shopId
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		}
	};
	return service;
});

angular.module('pHome').service('marketService', function($http) {
	var service = {

		getSalesTop10: function(time) {
			return $http({
				method: 'GET',
				url: '/api/home/market/sales-top10',
				params: {
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getGoodTradeTop10: function(time) {
			return $http({
				method: 'GET',
				url: '/api/home/market/good-trade-top10',
				params: {
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getShopFlowTop10: function(time) {
			return $http({
				method: 'GET',
				url: '/api/home/market/shop-flow-top10',
				params: {
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getGoodViewTop10: function(time) {
			return $http({
				method: 'GET',
				url: '/api/home/market/good-view-top10',
				params: {
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getSearchKeywordTop10: function(time) {
			return $http({
				method: 'GET',
				url: '/api/home/market/search-keyword-top10',
				params: {
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		}
	};

	return service;
});

angular.module('pHome').service('flowService', function($http) {

	var service = {
		getBounceRate: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/flows/bounce-rate',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getAverageStayTime: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/flows/average-stay-time',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getAveragePV: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/flows/average-pv',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getFlows: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/flows',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		}
	};

	return service;
});

angular.module('pHome').service('tradeService', function($http) {
	var service = {
		getTrade: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/trade',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getOrderUserCount: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/trade/order-user-count',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getPayedUserCount: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/trade/paid-user-count',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getPayedGoodCount: function(shopId, time) {
			return $http({
				method: 'GET',
				url: '/api/home/trade/paid-good-count',
				params: {
					shop_id: shopId,
					time: time
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		}
	};

	return service;
});

angular.module('pHome').service('goodService', function($http) {

	var service = {
		getGoodData: function(shopId) {
			return $http({
				method: 'GET',
				url: '/api/home/good',
				params: {
					shop_id: shopId
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		}
	};

	return service;
});

angular.module('pMonitor').service('monitorService', function($http) {

	return {
		addMonitorShop: function(mainShopId, monitorShopName) {
			return $http({
				method: 'POST',
				url: '/monitors',
				params: {
					mainShopId: mainShopId
				},
				data: {
					mainShopId: mainShopId,
					monitorShopName: monitorShopName
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		},
		getMonitorShops: function(mainShopId) {
			return $http({
				method: 'GET',
				url: '/monitors/' + mainShopId
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			})
		}
	};

});

angular.module('pMonitor').service('detailService', function($http) {
	return {

	}
});

angular.module('pHome').service('customerService', function($http) {
	return {
		getCustomerClassification: function(shopId) {
			return $http({
				method: 'GET',
				url: '/api/home/customer',
				params: {
					shop_id: shopId
				}
			}).then(function(response) {
				return response.data;
			}, function(response) {
				return response.data;
			});
		}
	};
});

angular.module('pHome').service('pNavService', function($http) {

	return {
		logout: function() {
			return $http({
				method: 'POST',
				url: '/logout',
			}).then(function(res){
				console.log(res.data.success);
				if(res.data.success){
					window.location.href = '/login.html';
				}
			},function(res){
				
			});
		}
	};
});