var pocket = angular.module('pocket',
[
'720kb.datepicker',
'ui.router',
'pHome',
'pCore',
'pMonitor',
'pVisual',
'percentage',
'app.common'
//'ui.select'
//'ngSanitize'
]);

var pHome = angular.module('pHome',[]);

var pNavbar = angular.module('pCore',[]);

var pMonitor = angular.module('pMonitor',['app.common']);

var pVisual = angular.module('pVisual',[]);
