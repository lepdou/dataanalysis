


angular.module('pCore').component('pNavbar',{
	templateUrl: 'p-navbar/p-navbar.html',
	controller: ['pNavService',function pNavbarCtrl(pNavService){
		var self = this;
		
		self.user = '张三'
		
		self.logout = function() {
			pNavService.logout();
		}

	}]
})