CREATE TABLE `category` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Name` varchar(500) NOT NULL DEFAULT 'default',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `IX_Name` (`Name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='行业表';

CREATE TABLE `favorite` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `GoodsId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `IX_GoodsId` (`GoodsId`),
  KEY `IX_UserId` (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=551 DEFAULT CHARSET=utf8 COMMENT='收藏表';

CREATE TABLE `goods` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Name` varchar(500) NOT NULL DEFAULT 'default',
  `Pic` varchar(500) DEFAULT NULL,
  `ShopId` int(11) NOT NULL,
  `Price` int(11) NOT NULL,
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`)
) ENGINE=InnoDB AUTO_INCREMENT=2961 DEFAULT CHARSET=utf8 COMMENT='商品表';

CREATE TABLE `order` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `GoodsId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Status` int(4) NOT NULL COMMENT '100:新建订单（未付款） 200:已付款（未发货） 300：交易成功（未评论） 301：交易成功（已评论）302：（交易成功）投诉中',
  `CreateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PayTime` timestamp NULL DEFAULT NULL,
  `Client` smallint(4) DEFAULT NULL COMMENT '100：app，200：web',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `IX_GoodsId` (`GoodsId`),
  KEY `IX_UserId` (`UserId`),
  KEY `IX_Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=902 DEFAULT CHARSET=utf8 COMMENT='订单表';

CREATE TABLE `pv` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ShopId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:商户主页 2. 推荐页面 3. 商品详情页',
  `Client` int(11) DEFAULT NULL,
  `GoodsId` int(11) DEFAULT NULL,
  `ViewTime` int(11) DEFAULT '0' COMMENT '停留时长',
  `CreateTime` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `IX_GoodsId` (`GoodsId`),
  KEY `IX_ShopId` (`ShopId`),
  KEY `IX_UserId` (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=10071 DEFAULT CHARSET=utf8 COMMENT='用户浏览表';

CREATE TABLE `search` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `key` varchar(500) NOT NULL,
  `CateId` int(11) NOT NULL,
  `SearchTime` datetime DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `IX_CateId` (`CateId`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COMMENT='搜索记录表';

CREATE TABLE `shop` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Name` varchar(500) NOT NULL DEFAULT 'default',
  `Pic` varchar(128) DEFAULT NULL,
  `CateId` int(11) DEFAULT '0',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='商户表';

CREATE TABLE `shoppingcart` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `GoodsId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `IX_GoodsId` (`GoodsId`)
) ENGINE=InnoDB AUTO_INCREMENT=551 DEFAULT CHARSET=utf8 COMMENT='购物车';

CREATE TABLE `user` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Name` varchar(500) NOT NULL DEFAULT 'default',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- 以下是统计表

CREATE TABLE `catedailydata` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CateId` int(11) NOT NULL DEFAULT '0' COMMENT '行业ID',
  `Sales` int(11) NOT NULL DEFAULT '0' COMMENT '销售额',
  `PayedGoodsCount` int(11) NOT NULL DEFAULT '0' COMMENT '支付商品数',
  `PayedUserCount` int(11) NOT NULL DEFAULT '0' COMMENT '支付买家数',
  `PV` int(11) NOT NULL COMMENT '浏览量',
  `UV` int(11) NOT NULL COMMENT '浏览用户数',
  `Partition` date DEFAULT NULL COMMENT '按天分区',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `CateId` (`CateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='行业维度统计表';


CREATE TABLE `shopdailydata` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ShopId` int(11) NOT NULL DEFAULT '0' COMMENT '商户ID',
  `CateId` int(11) DEFAULT '0',
  `CreateOrderCount` int(11) DEFAULT NULL COMMENT '下单数',
  `Sales` int(11) NOT NULL DEFAULT '0' COMMENT '销售额',
  `PayedGoodsCount` int(11) NOT NULL DEFAULT '0' COMMENT '支付商品数',
  `ViewGoodsCount` int(11) DEFAULT NULL COMMENT '访问商品数',
  `PayedUserCount` int(11) NOT NULL DEFAULT '0' COMMENT '支付买家数',
  `PV` int(11) NOT NULL COMMENT '总浏览量',
  `PVByApp` int(11) DEFAULT NULL COMMENT '移动浏览量',
  `VisitOnlyOnePageUsers` int(11) DEFAULT '0' COMMENT '只浏览了一个页面的用户数',
  `UV` int(11) NOT NULL COMMENT '总浏览用户数',
  `UVByApp` int(11) DEFAULT NULL COMMENT '移动用户浏览数',
  `AddShoppingCartCount` int(11) DEFAULT '0' COMMENT '加入购物车次数',
  `AvgStayTime` int(11) DEFAULT NULL COMMENT '平均停留时间',
  `Partition` date DEFAULT NULL COMMENT '按天分区',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `ShopId` (`ShopId`)
) ENGINE=InnoDB AUTO_INCREMENT=617 DEFAULT CHARSET=utf8 COMMENT='商户维度统计表';

CREATE TABLE `goodsdailydata` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `GoodsId` int(11) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `ShopId` int(11) NOT NULL DEFAULT '0',
  `CateId` int(11) DEFAULT '0',
  `FavoriteCount` int(11) NOT NULL DEFAULT '0' COMMENT '收藏数',
  `AddToCartCount` int(11) NOT NULL DEFAULT '0' COMMENT '加入购物车数',
  `PayCount` int(11) NOT NULL COMMENT '购买量',
  `PayMoneyCount` bigint(20) NOT NULL COMMENT '购买总金额',
  `PV` int(11) NOT NULL DEFAULT '0' COMMENT '访问量',
  `Partition` date DEFAULT NULL COMMENT '按天分区',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `GoodsId` (`GoodsId`)
) ENGINE=InnoDB AUTO_INCREMENT=1661 DEFAULT CHARSET=utf8 COMMENT='商品维度统计表';

CREATE TABLE `monitor` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `MainShopId` int(11) NOT NULL DEFAULT '0' COMMENT '主体商户Id',
  `MonitorShopId` int(11) DEFAULT '0' COMMENT '监控的shopId',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='商户监控表';
